<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            'description' => 'Blanco',
            'color' => '255, 255, 255',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('colors')->insert([
            'description' => 'Azul',
            'color' => '95, 117, 178',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('colors')->insert([
            'description' => 'Amarillo',
            'color' => '186, 204, 40',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('colors')->insert([
            'description' => 'Rojo',
            'color' => '204, 40, 48',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
