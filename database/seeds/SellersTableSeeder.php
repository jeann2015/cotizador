<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SellersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;

        DB::table('sellers')->insert([
            'fname' => 'Juan',
            'lname' => 'Perez',
            'email' => 'juan@demo.com',
            'phone' => '123656555',
            'status'=>1,
            'type'=>1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('seller_offices')->insert([
            'sellers_id' => 1,
            'offices_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        $user->news_user_sellers("Juan Perez", "juan@demo.com", 2, 1);


        DB::table('sellers')->insert([
            'fname' => 'Maria',
            'lname' => 'Gomez',
            'email' => 'maria@demo.com',
            'phone' => '124446555',
            'status'=>1,
            'type'=>0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('seller_offices')->insert([
            'sellers_id' => 2,
            'offices_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $user->news_user_sellers("Maria Gomez", "maria@demo.com", 2, 2);

        DB::table('sellers')->insert([
            'fname' => 'Miguel',
            'lname' => 'Gutierrez',
            'email' => 'miguel@demo.com',
            'phone' => '552541555',
            'status'=>1,
            'type'=>0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('seller_offices')->insert([
            'sellers_id' => 3,
            'offices_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $user->news_user_sellers("Miguel Gutierrez", "miguel@demo.com", 2, 3);
    }
}
