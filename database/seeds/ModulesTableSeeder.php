<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'description' => 'Principal',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Principal',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Users',
            'order' => '2',
            'id_father' =>'1',
            'url' =>'users',
            'messages' => 'Users',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Audit',
            'order' => '3',
            'id_father' =>'1',
            'url' =>'audit',
            'messages' => 'Audit',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Modules',
            'order' => '4',
            'id_father' =>'1',
            'url' =>'modules',
            'messages' => 'Modules',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);



        DB::table('modules')->insert([
            'description' => 'Reports',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Reports',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reports General',
            'order' => '1',
            'id_father' =>'5',
            'url' =>'reports',
            'messages' => 'Reports General',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        DB::table('modules')->insert([
            'description' => 'Grupos',
            'order' => '7',
            'id_father' =>'1',
            'url' =>'grupos',
            'messages' => 'Grupos',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);



        DB::table('modules')->insert([
            'description' => 'Marcas',
            'order' => '8',
            'id_father' =>'1',
            'url' =>'marks',
            'messages' => 'Marcas',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Modelos',
            'order' => '9',
            'id_father' =>'1',
            'url' =>'patterns',
            'messages' => 'Modelos',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

//        DB::table('modules')->insert([
//            'description' => 'Cabinas',
//            'order' => '10',
//            'id_father' =>'1',
//            'url' =>'cabins',
//            'messages' => 'Cabinas',
//            'status' =>'1',
//            'visible' =>'1',
//            'clase' =>'pages',
//            'created_at' => Carbon::now(),
//            'updated_at' => Carbon::now()
//        ]);

//        DB::table('modules')->insert([
//            'description' => 'Traccion',
//            'order' => '11',
//            'id_father' =>'1',
//            'url' =>'tractions',
//            'messages' => 'Traccion',
//            'status' =>'1',
//            'visible' =>'1',
//            'clase' =>'pages',
//            'created_at' => Carbon::now(),
//            'updated_at' => Carbon::now()
//        ]);

        DB::table('modules')->insert([
            'description' => 'Sucursales',
            'order' => '12',
            'id_father' =>'1',
            'url' =>'offices',
            'messages' => 'Sucursales',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Vendedores',
            'order' => '13',
            'id_father' =>'1',
            'url' =>'sellers',
            'messages' => 'Vendedores',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Colores',
            'order' => '14',
            'id_father' =>'1',
            'url' =>'colors',
            'messages' => 'Colores',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Cotizaciones',
            'order' => '15',
            'id_father' =>'1',
            'url' =>'cotizaciones',
            'messages' => 'Cotizaciones',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Cambio de Clave',
            'order' => '16',
            'id_father' =>'1',
            'url' =>'changes',
            'messages' => 'Cambio de Clave',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

//        DB::table('modules')->insert([
//            'description' => 'Vehiculos',
//            'order' => '15',
//            'id_father' =>'1',
//            'url' =>'vehicles',
//            'messages' => 'Vehiculos',
//            'status' =>'1',
//            'visible' =>'1',
//            'clase' =>'pages',
//            'created_at' => Carbon::now(),
//            'updated_at' => Carbon::now()
//        ]);
    }
}
