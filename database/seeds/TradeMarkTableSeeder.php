<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TradeMarkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trade_marks')->insert([
            'description' => 'Isuzu',
            'facebook'=>'-',
            'twitter'=>'-',
            'instagram'=>'-',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('trade_marks')->insert([
            'description' => 'MG',
            'facebook'=>'-',
            'twitter'=>'-',
            'instagram'=>'-',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
