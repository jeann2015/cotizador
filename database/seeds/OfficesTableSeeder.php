<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offices')->insert([
            'name' => 'Juan Diaz',
            'address' => 'Juan Diaz',
            'schedule' => '8am  a 6 pm',
            'lat'=>'15824.2563',
            'lon'=>'-15824.2563',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('office_traders')->insert([
            'offices_id' => 1,
            'trade_marks_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
