<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicles_id')->unsigned();
            $table->timestamps();

            $table->foreign('vehicles_id')->references('id')->on('vehicles');
        });

        \DB::statement("ALTER TABLE vehicle_imagens ADD COLUMN `images`  LONGBLOB AFTER `vehicles_id`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_imagens');
    }
}
