<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesPatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes_patterns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotes_id')->unsigned();
            $table->integer('patterns_id')->unsigned();
            $table->integer('sellers_id')->unsigned();
            $table->timestamps();
            $table->foreign('quotes_id')->references('id')->on('quotes');
            $table->foreign('patterns_id')->references('id')->on('patterns');
            $table->foreign('sellers_id')->references('id')->on('sellers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes_patterns');
    }
}
