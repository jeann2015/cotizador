<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname', 35);
            $table->string('lname', 35);
            $table->string('email', 35);
            $table->string('phone', 25);
            $table->string('id_card', 25);
            $table->boolean('contacted')->default(0);
            $table->integer('offices_id')->unsigned();
            $table->timestamps();

            $table->foreign('offices_id')->references('id')->on('offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
