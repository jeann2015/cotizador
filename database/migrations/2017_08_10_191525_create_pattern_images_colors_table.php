<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternImagesColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern_images_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patterns_id')->unsigned();
            $table->integer('sub_patterns_id')->unsigned();
            $table->integer('colors_id')->unsigned();
            $table->timestamps();

            $table->foreign('patterns_id')->references('id')->on('patterns');
            $table->foreign('sub_patterns_id')->references('id')->on('pattern_details');
            $table->foreign('colors_id')->references('id')->on('colors');
        });

        \DB::statement("ALTER TABLE pattern_images_colors ADD COLUMN `images`  LONGBLOB AFTER `colors_id`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pattern_images_colors');
    }
}
