<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('trade_marks_id')->unsigned();
            $table->integer('patterns_id')->unsigned();
            $table->integer('cabins_id')->unsigned();
            $table->integer('tractions_id')->unsigned();
            $table->timestamps();

            $table->foreign('trade_marks_id')->references('id')->on('trade_marks');
            $table->foreign('patterns_id')->references('id')->on('patterns');
            $table->foreign('cabins_id')->references('id')->on('cabins');
            $table->foreign('tractions_id')->references('id')->on('tractions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
