<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMarkFieldAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_marks', function (Blueprint $table) {
            $table->string('facebook', 81)->after('description')->default('-');
            $table->string('twitter', 81)->after('facebook')->default('-');
            $table->string('instagram', 81)->after('twitter')->default('-');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
