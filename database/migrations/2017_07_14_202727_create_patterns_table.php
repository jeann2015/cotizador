<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patterns', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('trade_marks_id')->unsigned();
            $table->timestamps();

            $table->foreign('trade_marks_id')->references('id')->on('trade_marks');
        });
        \DB::statement("ALTER TABLE patterns ADD COLUMN `images1`  LONGBLOB AFTER `trade_marks_id`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patterns');
    }
}
