<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sellers_id')->unsigned();
            $table->integer('offices_id')->unsigned();
            $table->timestamps();

            $table->foreign('sellers_id')->references('id')->on('sellers');
            $table->foreign('offices_id')->references('id')->on('offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_offices');
    }
}
