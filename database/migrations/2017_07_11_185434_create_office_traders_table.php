<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeTradersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_traders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offices_id')->unsigned();
            $table->integer('trade_marks_id')->unsigned();
            $table->timestamps();

            $table->foreign('offices_id')->references('id')->on('offices');
            $table->foreign('trade_marks_id')->references('id')->on('trade_marks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_traders');
    }
}
