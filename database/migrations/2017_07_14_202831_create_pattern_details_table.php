<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('features');
            $table->integer('patterns_id')->unsigned();
            $table->integer('colors_id')->unsigned();
            $table->timestamps();

            $table->foreign('patterns_id')->references('id')->on('patterns');
            $table->foreign('colors_id')->references('id')->on('colors');
        });

        \DB::statement("ALTER TABLE pattern_details ADD COLUMN `images1`  LONGBLOB AFTER `colors_id`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pattern_details');
    }
}
