<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubPatternsFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_pattern_details_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patterns_id')->unsigned();
            $table->integer('sub_patterns_id')->unsigned();
            $table->string('type_file', 80);
            $table->integer('size')->default(0);
            $table->timestamps();

            $table->foreign('patterns_id')->references('id')->on('patterns');
            $table->foreign('sub_patterns_id')->references('id')->on('pattern_details');
        });
        \DB::statement("ALTER TABLE sub_pattern_details_files ADD COLUMN `files`  LONGBLOB AFTER `size`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_pattern_details_files');
    }
}
