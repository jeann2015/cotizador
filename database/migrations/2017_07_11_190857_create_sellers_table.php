<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname', 35);
            $table->string('lname', 35);
            $table->string('email', 191);
            $table->boolean('status')->default(1);
            $table->boolean('type')->default(0);
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE sellers ADD COLUMN `images`  LONGBLOB AFTER `status`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
