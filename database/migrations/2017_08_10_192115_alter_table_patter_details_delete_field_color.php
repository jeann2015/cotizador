<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePatterDetailsDeleteFieldColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pattern_details', function (Blueprint $table) {
            $table->dropForeign('pattern_details_colors_id_foreign');
            $table->dropColumn(['colors_id']);
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('pattern_details', function (Blueprint $table) {
//            $table->integer('colors_id')->after('prize')->unsigned();
//            $table->foreign('colors_id')->references('id')->on('colors');
//        });
    }
}
