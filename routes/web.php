<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Quotes;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/modules', 'ModulesController@index');
Route::get('/modules/add', 'ModulesController@add');
Route::post('/modules/new', 'ModulesController@news');
Route::get('/modules/edit/{id}', 'ModulesController@edit');
Route::post('/modules/update', 'ModulesController@update');
Route::get('/modules/delete/{id}', 'ModulesController@delete');
Route::post('/modules/destroy', 'ModulesController@destroy');

Route::get('/audit', 'AuditsController@index');

Route::get('/home', 'HomeController@index');
Route::get('/users', 'UsersController@index');
Route::get('/users/add', 'UsersController@add');
Route::post('/users/new', 'UsersController@news');
Route::get('/users/edit/{id}', 'UsersController@edit');
Route::post('/users/update', 'UsersController@update');
Route::get('/users/delete/{id}', 'UsersController@delete');
Route::post('/users/destroy', 'UsersController@destroy');

Route::get('/grupos', 'GruposController@index');
Route::get('/grupos/add', 'GruposController@add');
Route::post('/grupos/new', 'GruposController@news');
Route::get('/grupos/edit/{id}', 'GruposController@edit');
Route::post('/grupos/update', 'GruposController@update');
Route::get('/grupos/delete/{id}', 'GruposController@delete');
Route::post('/grupos/destroy', 'GruposController@destroy');

//Route::get('/cabins', 'CabinController@index');
//Route::get('/cabins/add', 'CabinController@add');
//Route::post('/cabins/new', 'CabinController@news');
//Route::get('/cabins/edit/{id}', 'CabinController@edit');
//Route::post('/cabins/update', 'CabinController@update');
//Route::get('/cabins/delete/{id}', 'CabinController@delete');
//Route::post('/cabins/destroy', 'CabinController@destroy');
//
//Route::get('/tractions', 'TractionController@index');
//Route::get('/tractions/add', 'TractionController@add');
//Route::post('/tractions/new', 'TractionController@news');
//Route::get('/tractions/edit/{id}', 'TractionController@edit');
//Route::post('/tractions/update', 'TractionController@update');
//Route::get('/tractions/delete/{id}', 'TractionController@delete');
//Route::post('/tractions/destroy', 'TractionController@destroy');
//
//Route::get('/vehicles', 'VehicleController@index');
//Route::get('/vehicles/add', 'VehicleController@add');
//Route::post('/vehicles/new', 'VehicleController@news');
//Route::get('/vehicles/edit/{id}', 'VehicleController@edit');
//Route::post('/vehicles/update', 'VehicleController@update');
//Route::get('/vehicles/delete/{id}', 'VehicleController@delete');
//Route::post('/vehicles/destroy', 'VehicleController@destroy');

Route::get('/patterns', 'PatternController@index');
Route::get('/patterns/add', 'PatternController@add');
Route::post('/patterns/new', 'PatternController@news');
Route::get('/patterns/edit/{id}', 'PatternController@edit');
Route::post('/patterns/update', 'PatternController@update');
Route::get('/patterns/delete/{id}', 'PatternController@delete');
Route::post('/patterns/destroy', 'PatternController@destroy');

Route::get('/patterns/addsub/{id}', 'PatternController@addsub');
Route::post('/patterns/newsub', 'PatternController@newsub');
Route::get('/patterns/editsub/{idmodel}/{id}', 'PatternController@editsub');
Route::post('/patterns/updatesub', 'PatternController@updatesub');
Route::get('/patterns/deletesub/{idmodel}/{id}', 'PatternController@deletesub');
Route::post('/patterns/destroysub', 'PatternController@destroysub');

Route::get('/offices', 'OfficeController@index');
Route::get('/offices/add', 'OfficeController@add');
Route::post('/offices/new', 'OfficeController@news');
Route::get('/offices/edit/{id}', 'OfficeController@edit');
Route::post('/offices/update', 'OfficeController@update');
Route::get('/offices/delete/{id}', 'OfficeController@delete');
Route::post('/offices/destroy', 'OfficeController@destroy');

Route::get('/sellers', 'SellerController@index');
Route::get('/sellers/add', 'SellerController@add');
Route::post('/sellers/new', 'SellerController@news');
Route::get('/sellers/edit/{id}', 'SellerController@edit');
Route::post('/sellers/update', 'SellerController@update');
Route::get('/sellers/delete/{id}', 'SellerController@delete');
Route::post('/sellers/destroy', 'SellerController@destroy');

Route::get('/marks', 'TradeMarkController@index');
Route::get('/marks/add', 'TradeMarkController@add');
Route::post('/marks/new', 'TradeMarkController@news');
Route::get('/marks/edit/{id}', 'TradeMarkController@edit');
Route::post('/marks/update', 'TradeMarkController@update');
Route::get('/marks/delete/{id}', 'TradeMarkController@delete');
Route::post('/marks/destroy', 'TradeMarkController@destroy');

Route::get('/colors', 'ColorController@index');
Route::get('/colors/add', 'ColorController@add');
Route::post('/colors/new', 'ColorController@news');
Route::get('/colors/edit/{id}', 'ColorController@edit');
Route::post('/colors/update', 'ColorController@update');
Route::get('/colors/delete/{id}', 'ColorController@delete');
Route::post('/colors/destroy', 'ColorController@destroy');

Route::get('/quotes/askfor', 'QuotesCustomController@askFor');
Route::get('/quotes/askformg', 'QuotesCustomController@askFormg');
Route::get('/quotes/details/{id}', 'QuotesCustomController@details');
Route::get('/quotes/send', 'QuotesCustomController@send');
Route::post('/quotes/new', 'QuotesCustomController@news');

Route::get('/cotizaciones', 'CotizacionesController@index');
Route::get('/cotizaciones/view/{id}', 'CotizacionesController@view');

Route::get('/reports', 'ReportsController@reports');
Route::post('/reports/search', 'ReportsController@search');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/changes', 'HomeController@changes');
Route::post('/changes/update', 'HomeController@update');

Route::get('/documents/download/{patterns_id}/{sub_patterns_id}/{name}/{type}/{quotes_id}', function ($patterns_id,$sub_patterns_id,$name,$type,$quotes_id)
{
    $quotes = Quotes::join('quotes_patterns', 'quotes_patterns.quotes_id', 'quotes.id')
        ->join('patterns', 'quotes_patterns.patterns_id', 'patterns.id')
        ->select('quotes_patterns.sellers_id', 'quotes.fname','quotes.lname','quotes.created_at',
             'patterns.id as patterns_id', 'patterns.description')
        ->where('quotes.id', '=', $quotes_id)->get();

    foreach ($quotes as $quote){
        $name_customer = $quote->fname." ".$quote->lname;
        $date_quotes = $quote->created_at;
    }

        $documents = \App\SubPatternDetailsFiles::where('patterns_id',$patterns_id)
        ->where('sub_patterns_id',$sub_patterns_id)
        ->select('files')
        ->get();

        foreach ($documents as $document) {
            $data = base64_decode($document->files);
            $type_file = $document->type_file;
        }

        if($type==1) {
            file_put_contents('my.pdf', $data);

            $pdf = new FPDI();

            $pdf->AddPage();

            $pdf->setSourceFile('my.pdf');

            $tplIdx = $pdf->importPage(1);
            $pdf->useTemplate($tplIdx, 10, 10, 190);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY(130, 50);
            $pdf->Write(0, $name);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY(155, 64);
            $pdf->Write(0, $quotes_id);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetXY(28, 50);
            $pdf->Write(0, $name_customer);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetXY(95, 49);
            $pdf->Write(0, date_format($date_quotes,"Y-m-d"));

            $pdf->Output();
        }else {

            file_put_contents('my.pdf', $data);

            $pdf = new FPDI();

            $pdf->AddPage();

            $pdf->setSourceFile('my.pdf');

            $tplIdx = $pdf->importPage(1);
            $pdf->useTemplate($tplIdx, 10, 10, 190);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY(130, 50);
            $pdf->Write(0, $name);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0,0,0);
            $pdf->SetXY(155, 64);
            $pdf->Write(0, $quotes_id);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetXY(28, 50);
            $pdf->Write(0, $name_customer);

            $pdf->SetFont('Helvetica');
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetXY(95, 49);
            $pdf->Write(0, date_format($date_quotes,"Y-m-d"));

            $pdf->Output("pdf.pdf","D");

        }

});
