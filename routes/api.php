<?php

use Illuminate\Http\Request;
use App\Eventos;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('/search/patterns/{trade_marks_id}', function (Request $request) {
    $patterns = \DB::table('patterns')
        ->join('pattern_details', 'pattern_details.pattern_id', '=', 'patterns.id')
        ->where('pattern_details.trade_marks_id', '=', $request->trade_marks_id)
        ->select(
            'patterns.id',
            'patterns.description'
        )
        ->pluck('patterns.description', 'patterns.id');
    return response()->json($patterns);
});

Route::get('/search/sellers/{offices_id}', function (Request $request) {
    $offices_id = $request->offices_id;
    if($offices_id<>"") {
        $seller = \DB::table('seller_offices')
            ->join('sellers', 'sellers.id', '=', 'seller_offices.sellers_id')
            ->where('seller_offices.offices_id', $offices_id)
            ->select(
                'sellers.id as sellers_id',
                \DB::raw('concat(sellers.fname," ",sellers.lname) as name_sellers')
            )
            ->pluck('name_sellers', 'sellers_id')->put('0', 'Todos Los Vendedores');

        $sellers = array_sort_recursive($seller->toArray());

        return response()->json($sellers);
    }else{
        return response()->json(array());
    }

});
