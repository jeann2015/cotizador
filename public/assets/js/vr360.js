
function Set360VR(selector, totalImages){
  if(!($(selector).length)){return false;}  // 요소가 없으면 실행하지 않음
  var imageNow = totalImages;   // 가장 첫 이미지가 제일 마지막 이미지 임(0도)
  var rotateInterval;
  var onPlaying = 0;
  var carColorNow = 0;
  var counting=0;
  var numColor = $(selector).find('dl.color dd').length;

  var totalImages = totalImages ? totalImages : 5;

  for(i=0;i<numColor;i++){
    var colorCode = $(selector).find('dl.color dd:eq('+i+') span.colorCode').text();
	$(selector).find('dl.color dd:eq('+i+') span.color').css({'background':colorCode});
  }

  $(selector).find('dl.color').css({'width':numColor*28})

  var loadimg=0;
  
  SetNewCar(0, 0, 0);  // 초기에는 1번색을 불러옴
  
  // 차량 컬러 변경
  $(selector).find('dl.color dd a').bind('click',function(){
    var index = $(selector).find('dl.color dd a').index(this);
	if(loadimg==0){
		SetNewCar(index,counting,0);
	}else{
		SetNewCar(index,counting,-1);
	}
  });
  // 이벤트(select color)
  var colorOverNow = 1;
  $(selector).find('dl.color > dd > a').bind('mouseenter focusin touchstart',function(){
    var index = $(selector).find('dl.color dd').index($(this).parent());
	if(carColorNow == index){return false;}	
	$(this).stop().animate({'width':'25px', 'height':'25px', 'top':'0px'},200);
	$(selector).find('dl.color dd:eq('+carColorNow+') a').stop().animate({'width':'23px', 'height':'23px', 'top':'2px'},200);
	colorOverNow = index;
  }).bind('mouseleave focusout touchend touchcancel',function(){
    if(carColorNow == colorOverNow){return false;}
	$(this).stop().animate({'width':'23px', 'height':'23px', 'top':'2px'},200);
	$(selector).find('dl.color dd:eq('+carColorNow+') a').stop().animate({'width':'25px', 'height':'25px', 'top':'0px'},200);
  });  
  // 이벤트(클릭)  
  $(selector).find('ul.rotate li.prev a').bind('click', function(){  // 왼쪽으로 회전
	loadimg=1;
	if(onPlaying == 1){return false;}
	RotateCar('left', 2, 30);
  });
  $(selector).find('ul.rotate li.next a').bind('click', function(){  // 오른쪽으로 회전
	loadimg=1;
	if(onPlaying == 1){return false;}
	RotateCar('right', 2, 30);
  });
  
  $(selector).find('ul.rotate li a').bind('mouseenter focusin touchstart', function(){
	$(this).find('img.over').css({'display':'block'});
  }).bind('mouseleave focusout touchend touchcancel', function(){
    $(this).find('img.over').css({'display':'none'});
  });
  
  // 이벤트(드래그)  ul.vrImage는 고정된 내용으로 이벤트 한번만 부여
  $(selector).find('ul.vrImage').bind('mousedown touchstart', function(e){
	e.preventDefault();
	clearInterval(rotateInterval);
	var startX = e.pageX;;;
	loadimg=1;
	$(document).bind('mousemove touchmove', function(e){	 
	  e.preventDefault();
	  if(onPlaying == 1){return false;}
	  if(startX - e.pageX > 5){
		ShowNext('left'); startX = e.pageX;
	  }else if(startX - e.pageX < -5){
		ShowNext('right'); startX = e.pageX;
	  }
	}).bind('mouseup touchend touchcancel', function(){
	  $(document).unbind('mousemove touchmove');
	});
  });
  $(selector).find('ul.vrImage').bind('mouseenter', function(){
    $(selector).find('p.indicator span.drag').stop().animate({'opacity':1});
  }).bind('mouseleave', function(){
    $(selector).find('p.indicator span.drag').stop().animate({'opacity':0});
  });
  var rotateuse=0;
  // 이벤트(select direction / front, side, rear)
  $(selector).find('.car-position li').bind('click', function(){
    //alert(imageNow);
	var index = $(selector).find('.car-position li').index(this);
	var destination = totalImages - index*15;   // 61(front), 46(side), 31(Rear), 16(side), 1(front)
	var destination2 = totalImages - index*15;   // 61(front), 46(side), 31(Rear), 16(side), 1(front)
	//alert(destination); alert(imageNow);
	loadimg=1;
	if(rotateuse==0){
		if($(this).index()==0){
			if(destination > imageNow){
			  var numPageMove = destination2 - imageNow;
			  RotateCar('left', numPageMove, 15);
			}else if(destination < imageNow){
			  var numPageMove = imageNow - destination2;
			  RotateCar('right', numPageMove, 15);
			}
		}else{
			if(destination > imageNow){
			  var numPageMove = destination - imageNow;
			  RotateCar('left', numPageMove, 15);
			}else if(destination < imageNow){
			  var numPageMove = imageNow - destination;
			  RotateCar('right', numPageMove, 15);
			}
		}
	}
  });
  
  
  function SetNewCar(n,view,m){  // n번째 컬러의 차량을 표시
	   carColorNow = -1;
    if(carColorNow == n){return false;}
	
	// 새로운 환경 설정
	imageNow = totalImages;
    onPlaying = 0;
	clearInterval(rotateInterval);
	
	// 상태 초기화(로딩바, 각도표시, 컬러표시 등)
	//$(selector).find('p.indicator span.barLoading').stop().css({'width':0});
	$(selector).find('ul.vrImage').html('');   // 기존 내용 삭제
	$(selector).find('p.indicator span.barDegree').css({'width':0});
	$(selector).find('p.indicator span.degree').css({'left':'-26px'});
	$(selector).find('p.indicator span.degree').text('0°');
	$(selector).find('ul.angle li a').css({'color':'#767676', 'font-weight':'normal'});
	$(selector).find('ul.angle li.indicator1 a').css({'color':'#000000', 'font-weight':'bold'});
	$(selector).find('dl.color dd:eq('+n+') a').stop().animate({'width':'25px', 'height':'25px', 'top':'-2px'},200);
	
	//새로운 이미지를 불러움(ul.color li span.path 경로 참조)
	var path = $(selector).find('dl.color dd:eq('+n+') span.path').text();
	var colorName = $(selector).find('dl.color dd:eq('+n+') img').attr('alt');
	$(selector).find('.control .colorName').text(colorName);  // 컬러이름 변경
	var htmlContent = '';
	for(i=0;i<totalImages;i++){
	  var num = 0
	  if(i<10){num = '00'+i.toString();}
	  else if(i<100){num = '0'+i.toString();}  // 0을 붙여서 3자리로 만듬
	  htmlContent += '<li><img alt="" src="'+path+num+'.jpg" /></li>'
	}
	$(selector).find('ul.vrImage').html(htmlContent);
	$(selector).find('ul.vrImage li').css({'z-index':0, 'opacity':0});
	$(selector).find('ul.vrImage li:eq('+imageNow+')').css({'opacity':1});
	// 인트로 효과
	$(selector).find('ul.vrImage li:eq('+(view+m)+')').css({'z-index':10,'opacity':0}).delay(700).animate({'opacity':1},800,function(){
	  $(selector).find('ul.vrImage li').css({'opacity':1});
	});	  	 	
	
	carColorNow = n;
	imageNow = counting;
	
  }  // SetNewCar(n);
  
  function RotateCar(direction, n, interval){  // direction : 회전방향(left/right), n : 플래이 할 이미지 갯수
	var count = 0;
	onPlaying = 1;
	rotateInterval = setInterval(function(){
	  if(direction == 'left'){
	    var next = (imageNow+1) > totalImages ? 1 : imageNow+1;
	  }else if(direction == 'right'){
	    var next = (imageNow-1) < 1 ? totalImages : imageNow-1;
	  }
	  rotateuse=1;
	  $(selector).find('ul.vrImage li:eq('+(imageNow-1)+')').css({'z-index':0});
	  $(selector).find('ul.vrImage li:eq('+(next-1)+')').css({'z-index':11});
	  imageNow = next;
	  counting = next;
	  IndicateDegree();
	  count++
	  if(count == n){rotateuse=0;clearInterval(rotateInterval); onPlaying = 0;}
	}, interval);
  }

  function ShowNext(direction){  // direction : 다음 이미지의 방향(left / right)
	if(direction == 'left'){
	  var next = (imageNow+1) > totalImages ? 1 : imageNow+1;
	}else if(direction == 'right'){
	  var next = (imageNow-1) < 1 ? totalImages : imageNow-1;
	}
	$(selector).find('ul.vrImage li:eq('+(imageNow-1)+')').css({'z-index':0});
	$(selector).find('ul.vrImage li:eq('+(next-1)+')').css({'z-index':11});
	imageNow = next;
	counting = next;

	IndicateDegree();
  }  
  function IndicateDegree(){
    var width = (360-(imageNow-1)*4)*(960/360);
	var degree = parseInt(360-(imageNow-1)*4);
	$(selector).find('.car-position ul li').removeClass();
	if(degree <= 170 || degree > 320 && degree <= 360){$(selector).find('.car-position ul li#posi1').addClass('on');}
	else if(degree <= 235){$(selector).find('.car-position ul li#posi2').addClass('on');}
	else if(degree <= 320){$(selector).find('.car-position ul li#posi3').addClass('on');}
  }

}