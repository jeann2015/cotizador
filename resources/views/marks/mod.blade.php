@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Modificar Marca</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('marks') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						{!! Form::open(array('url' => 'marks/update')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$trademarks->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Facebook</label>
							{!! Form::text('facebook',$trademarks->facebook,array('class' => 'form-control','id'=>'facebook','required')) !!}
						</div>
						<div class="form-group">
							<label>Twitter</label>
							{!! Form::text('twitter',$trademarks->twitter,array('class' => 'form-control','id'=>'twitter','required')) !!}
						</div>
						<div class="form-group">
							<label>Instagram</label>
							{!! Form::text('instagram',$trademarks->instagram,array('class' => 'form-control','id'=>'instagram','required')) !!}
						</div>
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$trademarks->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection