    <?php $base = 'http'.(@$_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/'; echo "<base href=\"{$base}\">"; ?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" async="" src="http://www.google-analytics.com/plugins/ua/linkid.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"a3c6b76fff3c607ef47d9f1775748b20",petok:"340e75a0a97a051dca378392b730f14b7d13c78c-1500831375-1800",zone:"isuzupanama.com.pa",rocket:"a",apps:{}}];document.write('<script type="text/javascript" src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js"><'+'\/script>');}}catch(e){};
//]]>
</script><script type="text/javascript" src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js"></script><style type="text/css">.cf-hidden { display: none; } .cf-invisible { visibility: hidden; }</style><script data-module="cloudflare/rocket" id="cfjs_block_d9cfc100ca" onload="CloudFlare.__cfjs_block_d9cfc100ca_load()" onerror="CloudFlare.__cfjs_block_d9cfc100ca_error()" onreadystatechange="CloudFlare.__cfjs_block_d9cfc100ca_readystatechange()" type="text/javascript" src="js/rocket.js"></script>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<title>Cotización | ISUZU Panama</title>
 
<meta name="description" content="Por favor llena los campos de nuestro formulario online y pronto te haremos llegar la cotización.">
<link rel="canonical" href="http://isuzupanama.com.pa/cotizacion/">
<meta property="og:locale" content="es_ES">
<meta property="og:type" content="article">
<meta property="og:title" content="Cotización | ISUZU Panama">
<meta property="og:description" content="Por favor llena los campos de nuestro formulario online y pronto te haremos llegar la cotización.">
<meta property="og:url" content="http://isuzupanama.com.pa/cotizacion/">
<meta property="og:site_name" content="ISUZU Panama">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="Por favor llena los campos de nuestro formulario online y pronto te haremos llegar la cotización.">
<meta name="twitter:title" content="Cotización | ISUZU Panama">
 
<link rel="dns-prefetch" href="//s.w.org">
<link rel="alternate" type="application/rss+xml" title="ISUZU Panama » Feed" href="http://isuzupanama.com.pa/feed/">
<link rel="alternate" type="application/rss+xml" title="ISUZU Panama » RSS de los comentarios" href="http://isuzupanama.com.pa/comments/feed/">
<script type="text/rocketscript" data-rocketoptimized="true">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/isuzupanama.com.pa\/wp-includes\/js\/wp-emoji-release.min.js"}};
            !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
        </script>
<style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-0.1em!important;background:none!important;padding:0!important;}</style>
<link rel="stylesheet" id="popup-maker-site-css" href="http://isuzupanama.com.pa/wp-content/plugins/popup-maker/assets/css/site.min.css" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css" href="http://isuzupanama.com.pa/wp-content/plugins/contact-form-7/includes/css/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="cookielawinfo-style-css" href="http://isuzupanama.com.pa/wp-content/plugins/cookie-law-info/css/cli-style.css" type="text/css" media="all">
<link rel="stylesheet" id="fvp-frontend-css" href="http://isuzupanama.com.pa/wp-content/plugins/featured-video-plus/styles/frontend.css" type="text/css" media="all">
<link rel="stylesheet" id="pixgridder-css" href="http://isuzupanama.com.pa/wp-content/plugins/pixgridder/css/front-gridder.css" type="text/css" media="all">
<link rel="stylesheet" id="wp-lightbox-2.min.css-css" href="http://isuzupanama.com.pa/wp-content/plugins/wp-lightbox-2/styles/lightbox.min.css" type="text/css" media="all">
<link rel="stylesheet" id="storefront-style-css" href="http://isuzupanama.com.pa/wp-content/themes/copama/style.css" type="text/css" media="all">
<style id="storefront-style-inline-css" type="text/css">.main-navigation ul li a,.site-title a,ul.menu li a,.site-branding h1 a{color:#ffffff;}.main-navigation ul li a:hover,.site-title a:hover{color:#e6e6e6;}.site-header,.main-navigation ul ul,.secondary-navigation ul ul,.main-navigation ul.menu>li.menu-item-has-children:after,.secondary-navigation ul.menu ul,.main-navigation ul.menu ul,.main-navigation ul.nav-menu ul{background-color:#2c2d33;}p.site-description,ul.menu li.current-menu-item>a{color:#9aa0a7;}h1,h2,h3,h4,h5,h6{color:#484c51;}.hentry .entry-header{border-color:#484c51;}.widget h1{border-bottom-color:#484c51;}body,.secondary-navigation a,.widget-area .widget a,.onsale,#comments .comment-list .reply a,.pagination .page-numbers li .page-numbers:not(.current),.woocommerce-pagination .page-numbers li .page-numbers:not(.current){color:#60646c;}a{color:#000000;}a:focus,.button:focus,.button.alt:focus,.button.added_to_cart:focus,.button.wc-forward:focus,button:focus,input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus{outline-color:#000000;}button,input[type="button"],input[type="reset"],input[type="submit"],.button,.added_to_cart,.widget-area .widget a.button,.site-header-cart .widget_shopping_cart a.button{background-color:#60646c;border-color:#60646c;color:#ffffff;}button:hover,input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,.button:hover,.added_to_cart:hover,.widget-area .widget a.button:hover,.site-header-cart .widget_shopping_cart a.button:hover{background-color:#474b53;border-color:#474b53;color:#ffffff;}button.alt,input[type="button"].alt,input[type="reset"].alt,input[type="submit"].alt,.button.alt,.added_to_cart.alt,.widget-area .widget a.button.alt,.added_to_cart,.pagination .page-numbers li .page-numbers.current,.woocommerce-pagination .page-numbers li .page-numbers.current{background-color:#000000;border-color:#000000;color:#ffffff;}button.alt:hover,input[type="button"].alt:hover,input[type="reset"].alt:hover,input[type="submit"].alt:hover,.button.alt:hover,.added_to_cart.alt:hover,.widget-area .widget a.button.alt:hover,.added_to_cart:hover{background-color:#000000;border-color:#000000;color:#ffffff;}.site-footer{background-color:#e52429;color:#ffffff;}.site-footer a:not(.button){color:#ffffff;}.site-footer h1,.site-footer h2,.site-footer h3,.site-footer h4,.site-footer h5,.site-footer h6{color:#000000;}@media screen and ( min-width: 768px ) {.main-navigation ul.menu>li>ul{border-top-color:#2c2d33}}.secondary-navigation ul.menu a:hover{color:#b3b9c0;}.main-navigation ul.menu ul{background-color:#2c2d33;}.secondary-navigation ul.menu a{color:#9aa0a7;} }
</style>
<link rel="stylesheet" id="wordpress-popular-posts-css" href="http://isuzupanama.com.pa/wp-content/plugins/wordpress-popular-posts/style/wpp.css" type="text/css" media="all">
<link rel="stylesheet" id="fancybox-css" href="http://isuzupanama.com.pa/wp-content/plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min.css" type="text/css" media="screen">
<!-- This site uses the Google Analytics by MonsterInsights plugin v6.2.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script type="text/javascript" data-cfasync="false">
    /* Function to detect opted out users */
    function __gaTrackerIsOptedOut() {
        return document.cookie.indexOf(disableStr + '=true') > -1;
    }

    /* Disable tracking if the opt-out cookie exists. */
    var disableStr = 'ga-disable-UA-64545683-4';
    if ( __gaTrackerIsOptedOut() ) {
        window[disableStr] = true;
    }

    /* Opt-out function */
    function __gaTrackerOptout() {
      document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
      window[disableStr] = true;
    }

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

    __gaTracker('create', 'UA-64545683-4', 'auto');
    __gaTracker('set', 'forceSSL', true);
    __gaTracker('set', 'anonymizeIp', true);
    __gaTracker('require', 'displayfeatures');
    __gaTracker('require', 'linkid', 'linkid.js');
    __gaTracker('send','pageview');
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-includes/js/jquery/jquery.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-includes/js/jquery/jquery-migrate.min.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","is_debug_mode":"false","download_extensions":"doc,exe,js,pdf,ppt,tgz,zip,xls","inbound_paths":"","home_url":"http:\/\/isuzupanama.com.pa","track_download_as":"event","internal_label":"int","hash_tracking":"false"};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-content/plugins/cookie-law-info/js/cookielawinfo.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-content/plugins/featured-video-plus/js/jquery.fitvids.min.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var fvpdata = {"ajaxurl":"http:\/\/isuzupanama.com.pa\/wp-admin\/admin-ajax.php","nonce":"c1d0c34469","fitvids":"1","dynamic":"","overlay":"","opacity":"0.75","color":"b","width":"640"};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-content/plugins/featured-video-plus/js/frontend.min.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="http://isuzupanama.com.pa/wp-content/plugins/advanced-iframe/js/ai.js" data-rocketoptimized="true"></script>
<link rel="https://api.w.org/" href="http://isuzupanama.com.pa/wp-json/">
<link rel="shortlink" href="http://isuzupanama.com.pa/?p=474">
<link rel="alternate" type="application/json+oembed" href="http://isuzupanama.com.pa/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fisuzupanama.com.pa%2Fcotizacion%2F">
<link rel="alternate" type="text/xml+oembed" href="http://isuzupanama.com.pa/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fisuzupanama.com.pa%2Fcotizacion%2F&amp;format=xml">
<script type="text/rocketscript" data-rocketoptimized="true">
(function(url){
    if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
    var addEvent = function(evt, handler) {
        if (window.addEventListener) {
            document.addEventListener(evt, handler, false);
        } else if (window.attachEvent) {
            document.attachEvent('on' + evt, handler);
        }
    };
    var removeEvent = function(evt, handler) {
        if (window.removeEventListener) {
            document.removeEventListener(evt, handler, false);
        } else if (window.detachEvent) {
            document.detachEvent('on' + evt, handler);
        }
    };
    var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
    var logHuman = function() {
        var wfscr = document.createElement('script');
        wfscr.type = 'text/javascript';
        wfscr.async = true;
        wfscr.src = url + '&r=' + Math.random();
        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
        for (var i = 0; i < evts.length; i++) {
            removeEvent(evts[i], logHuman);
        }
    };
    for (var i = 0; i < evts.length; i++) {
        addEvent(evts[i], logHuman);
    }
})('//isuzupanama.com.pa/?wordfence_logHuman=1&hid=7CF41DEED41B8B5B60D8722D9246A015');
</script><!-- <meta name="NextGEN" version="2.2.3" /> -->
                <!-- WordPress Popular Posts v3.3.4 -->
                <script type="text/rocketscript" data-rocketoptimized="true">

                    var sampling_active = 0;
                    var sampling_rate   = 100;
                    var do_request = false;

                    if ( !sampling_active ) {
                        do_request = true;
                    } else {
                        var num = Math.floor(Math.random() * sampling_rate) + 1;
                        do_request = ( 1 === num );
                    }

                    if ( do_request ) {

                        /* Create XMLHttpRequest object and set variables */
                        var xhr = ( window.XMLHttpRequest )
                          ? new XMLHttpRequest()
                          : new ActiveXObject( "Microsoft.XMLHTTP" ),
                        url = 'http://isuzupanama.com.pa/wp-admin/admin-ajax.php',
                        params = 'action=update_views_ajax&token=afc2e7267b&wpp_id=474';
                        /* Set request method and target URL */
                        xhr.open( "POST", url, true );
                        /* Set request header */
                        xhr.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
                        /* Hook into onreadystatechange */
                        xhr.onreadystatechange = function() {
                            if ( 4 === xhr.readyState && 200 === xhr.status ) {
                                if ( window.console && window.console.log ) {
                                    window.console.log( xhr.responseText );
                                }
                            }
                        };
                        /* Send request */
                        xhr.send( params );

                    }

                </script>
                <!-- End WordPress Popular Posts v3.3.4 -->
                
<!-- Easy FancyBox 1.6 using FancyBox 1.3.8 - RavanH (http://status301.net/wordpress-plugins/easy-fancybox/) -->
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var fb_timeout = null;
var fb_opts = { 'overlayShow' : true, 'hideOnOverlayClick' : true, 'showCloseButton' : true, 'margin' : 20, 'centerOnScroll' : true, 'enableEscapeButton' : true, 'autoScale' : true };
var easy_fancybox_handler = function(){
    /* IMG */
    var fb_IMG_select = 'a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpg"]:not(.nolightbox), a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpeg"]:not(.nolightbox), a[href*=".png"]:not(.nolightbox,li.nolightbox>a), area[href*=".png"]:not(.nolightbox)';
    jQuery(fb_IMG_select).addClass('fancybox image');
    var fb_IMG_sections = jQuery('div.gallery');
    fb_IMG_sections.each(function() { jQuery(this).find(fb_IMG_select).attr('rel', 'gallery-' + fb_IMG_sections.index(this)); });
    jQuery('a.fancybox, area.fancybox, li.fancybox a').fancybox( jQuery.extend({}, fb_opts, { 'transitionIn' : 'elastic', 'easingIn' : 'easeOutBack', 'transitionOut' : 'elastic', 'easingOut' : 'easeInBack', 'opacity' : false, 'hideOnContentClick' : false, 'titleShow' : true, 'titlePosition' : 'over', 'titleFromAlt' : true, 'showNavArrows' : true, 'enableKeyboardNav' : true, 'cyclic' : false }) );
    /* iFrame */
    jQuery('a.fancybox-iframe, area.fancybox-iframe, li.fancybox-iframe a').fancybox( jQuery.extend({}, fb_opts, { 'type' : 'iframe', 'width' : '70%', 'height' : '90%', 'padding' : 0, 'titleShow' : false, 'titlePosition' : 'float', 'titleFromAlt' : true, 'allowfullscreen' : false }) );
}
var easy_fancybox_auto = function(){
    /* Auto-click */
    setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);
}
/* ]]> */
</script>
    <style id="pum-styles" type="text/css">
    /* Popup Google Fonts */
@import url('//fonts.googleapis.com/css?family=Acme|Montserrat');

/* Popup Theme 1135: Framed Border */
.pum-theme-1135, .pum-theme-framed-border { background-color: rgba( 255, 255, 255, 0.50 ) } 
.pum-theme-1135 .pum-container, .pum-theme-framed-border .pum-container { padding: 18px; border-radius: 0px; border: 20px outset #dd3333; box-shadow: 1px 1px 3px 0px rgba( 2, 2, 2, 0.97 ) inset; background-color: rgba( 255, 251, 239, 1.00 ) } 
.pum-theme-1135 .pum-title, .pum-theme-framed-border .pum-title { color: #000000; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: inherit; font-size: 32px; line-height: 36px } 
.pum-theme-1135 .pum-content, .pum-theme-framed-border .pum-content { color: #2d2d2d; font-family: inherit } 
.pum-theme-1135 .pum-content + .pum-close, .pum-theme-framed-border .pum-content + .pum-close { height: 20px; width: 20px; left: auto; right: -20px; bottom: auto; top: -20px; padding: 0px; color: #ffffff; font-family: Acme; font-size: 20px; line-height: 20px; border: 1px none #ffffff; border-radius: 0px; box-shadow: 0px 0px 0px 0px rgba( 2, 2, 2, 0.23 ); text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 ); background-color: rgba( 0, 0, 0, 0.55 ) } 

/* Popup Theme 1134: Cutting Edge */
.pum-theme-1134, .pum-theme-cutting-edge { background-color: rgba( 0, 0, 0, 0.50 ) } 
.pum-theme-1134 .pum-container, .pum-theme-cutting-edge .pum-container { padding: 18px; border-radius: 0px; border: 1px none #000000; box-shadow: 0px 10px 25px 0px rgba( 2, 2, 2, 0.50 ); background-color: rgba( 30, 115, 190, 1.00 ) } 
.pum-theme-1134 .pum-title, .pum-theme-cutting-edge .pum-title { color: #ffffff; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: Sans-Serif; font-size: 26px; line-height: 28px } 
.pum-theme-1134 .pum-content, .pum-theme-cutting-edge .pum-content { color: #ffffff; font-family: inherit } 
.pum-theme-1134 .pum-content + .pum-close, .pum-theme-cutting-edge .pum-content + .pum-close { height: 24px; width: 24px; left: auto; right: 0px; bottom: auto; top: 0px; padding: 0px; color: #1e73be; font-family: inherit; font-size: 32px; line-height: 24px; border: 1px none #ffffff; border-radius: 0px; box-shadow: -1px 1px 1px 0px rgba( 2, 2, 2, 0.10 ); text-shadow: -1px 1px 1px rgba( 0, 0, 0, 0.10 ); background-color: rgba( 238, 238, 34, 1.00 ) } 

/* Popup Theme 1133: Hello Box */
.pum-theme-1133, .pum-theme-hello-box { background-color: rgba( 0, 0, 0, 0.75 ) } 
.pum-theme-1133 .pum-container, .pum-theme-hello-box .pum-container { padding: 30px; border-radius: 80px; border: 14px solid #81d742; box-shadow: 0px 0px 0px 0px rgba( 2, 2, 2, 0.00 ); background-color: rgba( 255, 255, 255, 1.00 ) } 
.pum-theme-1133 .pum-title, .pum-theme-hello-box .pum-title { color: #2d2d2d; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: Montserrat; font-size: 32px; line-height: 36px } 
.pum-theme-1133 .pum-content, .pum-theme-hello-box .pum-content { color: #2d2d2d; font-family: inherit } 
.pum-theme-1133 .pum-content + .pum-close, .pum-theme-hello-box .pum-content + .pum-close { height: auto; width: auto; left: auto; right: -30px; bottom: auto; top: -30px; padding: 0px; color: #2d2d2d; font-family: inherit; font-size: 32px; line-height: 28px; border: 1px none #ffffff; border-radius: 28px; box-shadow: 0px 0px 0px 0px rgba( 2, 2, 2, 0.23 ); text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 ); background-color: rgba( 255, 255, 255, 1.00 ) } 

/* Popup Theme 1132: Enterprise Blue */
.pum-theme-1132, .pum-theme-enterprise-blue { background-color: rgba( 0, 0, 0, 0.70 ) } 
.pum-theme-1132 .pum-container, .pum-theme-enterprise-blue .pum-container { padding: 28px; border-radius: 5px; border: 1px none #000000; box-shadow: 0px 10px 25px 4px rgba( 2, 2, 2, 0.50 ); background-color: rgba( 255, 255, 255, 1.00 ) } 
.pum-theme-1132 .pum-title, .pum-theme-enterprise-blue .pum-title { color: #315b7c; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: inherit; font-size: 34px; line-height: 36px } 
.pum-theme-1132 .pum-content, .pum-theme-enterprise-blue .pum-content { color: #2d2d2d; font-family: inherit } 
.pum-theme-1132 .pum-content + .pum-close, .pum-theme-enterprise-blue .pum-content + .pum-close { height: 28px; width: 28px; left: auto; right: 8px; bottom: auto; top: 8px; padding: 4px; color: #ffffff; font-family: inherit; font-size: 20px; line-height: 20px; border: 1px none #ffffff; border-radius: 42px; box-shadow: 0px 0px 0px 0px rgba( 2, 2, 2, 0.23 ); text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 ); background-color: rgba( 49, 91, 124, 1.00 ) } 

/* Popup Theme 1131: Light Box */
.pum-theme-1131, .pum-theme-lightbox { background-color: rgba( 0, 0, 0, 0.60 ) } 
.pum-theme-1131 .pum-container, .pum-theme-lightbox .pum-container { padding: 18px; border-radius: 3px; border: 8px solid #000000; box-shadow: 0px 0px 30px 0px rgba( 2, 2, 2, 1.00 ); background-color: rgba( 255, 255, 255, 1.00 ) } 
.pum-theme-1131 .pum-title, .pum-theme-lightbox .pum-title { color: #000000; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: inherit; font-size: 32px; line-height: 36px } 
.pum-theme-1131 .pum-content, .pum-theme-lightbox .pum-content { color: #000000; font-family: inherit } 
.pum-theme-1131 .pum-content + .pum-close, .pum-theme-lightbox .pum-content + .pum-close { height: 30px; width: 30px; left: auto; right: -24px; bottom: auto; top: -24px; padding: 0px; color: #ffffff; font-family: inherit; font-size: 24px; line-height: 26px; border: 2px solid #ffffff; border-radius: 30px; box-shadow: 0px 0px 15px 1px rgba( 2, 2, 2, 0.75 ); text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 ); background-color: rgba( 0, 0, 0, 1.00 ) } 

/* Popup Theme 1130: Default Theme */
.pum-theme-1130, .pum-theme-default-theme { background-color: rgba( 255, 255, 255, 1.00 ) } 
.pum-theme-1130 .pum-container, .pum-theme-default-theme .pum-container { padding: 18px; border-radius: 0px; border: 1px none #000000; box-shadow: 1px 1px 3px 0px rgba( 2, 2, 2, 0.23 ); background-color: rgba( 249, 249, 249, 1.00 ) } 
.pum-theme-1130 .pum-title, .pum-theme-default-theme .pum-title { color: #000000; text-align: left; text-shadow: 0px 0px 0px rgba( 2, 2, 2, 0.23 ); font-family: inherit; font-weight: inherit; font-size: 32px; font-style: normal; line-height: 36px } 
.pum-theme-1130 .pum-content, .pum-theme-default-theme .pum-content { color: #8c8c8c; font-family: inherit; font-weight: inherit; font-style: normal } 
.pum-theme-1130 .pum-content + .pum-close, .pum-theme-default-theme .pum-content + .pum-close { height: auto; width: auto; left: auto; right: 0px; bottom: auto; top: 0px; padding: 8px; color: #ffffff; font-family: inherit; font-weight: inherit; font-size: 12px; font-style: normal; line-height: 14px; border: 1px none #ffffff; border-radius: 0px; box-shadow: 0px 0px 0px 0px rgba( 2, 2, 2, 0.23 ); text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 ); background-color: rgba( 0, 183, 205, 1.00 ) } 


    
        </style>
<link rel="stylesheet" id="metaslider-nivo-slider-css" href="http://isuzupanama.com.pa/wp-content/plugins/ml-slider/assets/sliders/nivoslider/nivo-slider.css" type="text/css" media="all" property="stylesheet">
<link rel="stylesheet" id="metaslider-public-css" href="http://isuzupanama.com.pa/wp-content/plugins/ml-slider/assets/metaslider/public.css" type="text/css" media="all" property="stylesheet">
<link rel="stylesheet" id="metaslider-nivo-slider-default-css" href="http://isuzupanama.com.pa/wp-content/plugins/ml-slider/assets/sliders/nivoslider/themes/default/default.css" type="text/css" media="all" property="stylesheet">
<script data-rocketsrc="http://isuzupanama.com.pa/wp-includes/js/wp-emoji-release.min.js" data-rocketoptimized="true" type="text/javascript" defer=""></script><script type="text/javascript" async="" data-rocketsrc="//isuzupanama.com.pa/?wordfence_logHuman=1&amp;hid=7CF41DEED41B8B5B60D8722D9246A015&amp;r=0.6457711779635313" data-rocketoptimized="true"></script><script type="text/javascript" src="http://isuzupanama.com.pa/?wordfence_logHuman=1&amp;hid=7CF41DEED41B8B5B60D8722D9246A015&amp;r=0.6457711779635313"></script>

<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Scripts -->

    <style>
        input.md-form-control {
            color: black;
        }
    </style>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">