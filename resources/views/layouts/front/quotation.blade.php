<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('layouts.front.head')
    </head>
    <body class="page-template-default page page-id-504 pixgridder no-wc-breadcrumb right-sidebar">
        <div id="page" class="hfeed site">
            <header  id="masthead" class="site-header" role="banner" >
                @include('layouts.front.header')
            </header>
            @yield('content')
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        {{--<script async defer--}}
        {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-QkencwLbIv0HJQdL5L3_jl6Ia3-1IDM&callback=initMap">--}}
        {{--</script>--}}

        <!-- tether.js -->
        <!-- tether.js -->
        <script src="assets/js/tether.min.js"></script>

        <!-- waves effects.js -->
        <script src="assets/plugins/waves/js/waves.min.js"></script>

        <!-- Custom js -->
        <script type="text/javascript" src="assets/pages/elements.js"></script>

        <!-- Scrollbar JS-->
        <script src="assets/plugins/slimscroll/js/jquery.slimscroll.js"></script>
        <script src="assets/plugins/slimscroll/js/jquery.nicescroll.min.js"></script>

        <script type="text/javascript" src="assets/pages/elements.js"></script>
    </body>
</html>
