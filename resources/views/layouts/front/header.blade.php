<nav class="">
    <div class="col-wrapp">
      <div class="col-full">
    
                <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
        <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
                        <a class="site-branding" href="http://isuzupanama.com.pa/" rel="home" title="ISUZU Panama"></a>
                                <nav class="secondary-navigation" role="navigation" aria-label="Secondary Navigation">
                    </nav><!-- #site-navigation -->
                <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Navigation">
        <button class="menu-toggle">Menú</button>
            <div class="primary-navigation">
                <ul id="menu-principal" class="menu nav-menu" aria-expanded="false">
                    <li id="menu-item-313" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-313"><a href="http://www.isuzupanama.com.pa/">Inicio</a></li>
                    <li id="menu-item-311" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311"><a href="http://isuzupanama.com.pa/sobre-isuzu/">Sobre Isuzu</a></li>
                    <li id="menu-item-314" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-314"><a href="#">Vehículos</a>
                        <ul class="sub-menu">
                            <li id="menu-item-877" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-877"><a href="http://isuzupanama.com.pa/camionetas/">Camionetas 4×4 (NUEVA)</a></li>
                            <li id="menu-item-437" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-437"><a href="http://isuzupanama.com.pa/pickups-de-trabajo/">Pickups de Trabajo</a></li>
                            <li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-317"><a href="http://isuzupanama.com.pa/pickups/">Pickups Doble Propósito</a></li>
                            <li id="menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-625"><a href="http://isuzupanama.com.pa/camiones-ligeros/">Camiones Línea Económica</a></li>
                            <li id="menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-637"><a href="http://isuzupanama.com.pa/camiones-ligeros-2/">Camiones Ligeros</a></li>
                            <li id="menu-item-642" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-642"><a href="http://isuzupanama.com.pa/camiones-semi-pesados/">Camiones Semi Pesados</a></li>
                            <li id="menu-item-805" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-805"><a href="http://isuzupanama.com.pa/camiones-para-aplicaciones-especiales/">Camiones Apl. Especiales</a></li>
                            <li id="menu-item-478" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-478"><a href="http://isuzupanama.com.pa/flota/">Flota</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-502" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-502"><a href="#">Ventas</a>
                        <ul class="sub-menu">
                            <li id="menu-item-479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-479"><a href="http://isuzupanama.com.pa/cotizacion/">Cotización</a></li>
                            <li id="menu-item-409" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409"><a href="http://isuzupanama.com.pa/test-drive/">Prueba de Manejo</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-503" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-503"><a href="#">Post-Venta</a>
                    <ul class="sub-menu">
                        <li id="menu-item-506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506"><a href="http://isuzupanama.com.pa/servicio-de-taller/">Servicio de taller</a></li>
                        <li id="menu-item-515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-515"><a href="http://isuzupanama.com.pa/repuestos/">Repuestos</a></li>
                        <li id="menu-item-513" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-513"><a href="http://isuzupanama.com.pa/garantia-de-vehiculos/">Garantía de Vehículos</a></li>
                        <li id="menu-item-514" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-514"><a href="http://isuzupanama.com.pa/garantia-de-servicio/">Garantía de Servicio</a></li>
                    </ul>
                    </li>
                    <li id="menu-item-492" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-492"><a href="http://isuzupanama.com.pa/contactenos/">Contáctenos</a></li>
                    </ul></div><div class="menu"><ul>
                    <li class="current_page_item"><a href="http://isuzupanama.com.pa/">Inicio</a></li><li class="page_item page-item-799"><a href="http://isuzupanama.com.pa/camiones-para-aplicaciones-especiales/">Camiones Apl. Especiales</a></li>
                    <li class="page_item page-item-627"><a href="http://isuzupanama.com.pa/camiones-ligeros-2/">Camiones Ligeros</a></li>
                    <li class="page_item page-item-452"><a href="http://isuzupanama.com.pa/camiones-ligeros/">Camiones Línea Económica</a></li>
                    <li class="page_item page-item-640"><a href="http://isuzupanama.com.pa/camiones-semi-pesados/">Camiones Semi Pesados</a></li>
                    <li class="page_item page-item-830"><a href="http://isuzupanama.com.pa/camionetas/">Camionetas 4×4 (NUEVA)</a></li>
                    <li class="page_item page-item-1066"><a href="http://isuzupanama.com.pa/campanas/">Campañas</a></li>
                    <li class="page_item page-item-481"><a href="http://isuzupanama.com.pa/contactenos/">Contáctenos</a></li>
                    <li class="page_item page-item-474"><a href="http://isuzupanama.com.pa/cotizacion/">Cotización</a></li>
                    <li class="page_item page-item-476"><a href="http://isuzupanama.com.pa/flota/">Flota</a></li>
                    <li class="page_item page-item-509"><a href="http://isuzupanama.com.pa/garantia-de-servicio/">Garantía de Servicio</a></li>
                    <li class="page_item page-item-511"><a href="http://isuzupanama.com.pa/garantia-de-vehiculos/">Garantía de Vehículos</a></li>
                    <li class="page_item page-item-280"><a href="http://isuzupanama.com.pa/inicio/">Inicio</a></li>
                    <li class="page_item page-item-435"><a href="http://isuzupanama.com.pa/pickups-de-trabajo/">Pickups de Trabajo</a></li>
                    <li class="page_item page-item-315"><a href="http://isuzupanama.com.pa/pickups/">Pickups Doble Propósito</a></li>
                    <li class="page_item page-item-407"><a href="http://isuzupanama.com.pa/test-drive/">Prueba de Manejo</a></li>
                    <li class="page_item page-item-507"><a href="http://isuzupanama.com.pa/repuestos/">Repuestos</a></li>
                    <li class="page_item page-item-504"><a href="http://isuzupanama.com.pa/servicio-de-taller/">Servicio de taller</a></li>
                    <li class="page_item page-item-309"><a href="http://isuzupanama.com.pa/sobre-isuzu/">Sobre Isuzu</a></li>
                </ul>
            </div>
        </nav><!-- #site-navigation -->                        
      </div>
    </div>
</nav>      