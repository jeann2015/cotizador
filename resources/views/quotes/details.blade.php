@extends('layouts.front.quotation')
@section('content')

<div class="container-fluid">
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
                    @if(count($pathToFileSeleer)>=1)
                        <img src="{{$pathToFileSeleer[1]}}" class="avatar">
                    @else
						<img src="assets/images/avatar-dev.png" class="avatar">
                    @endif
				</div>
				<div class="col-md-8 agent-info">
					<p>
						<span>Hola {{$data['customer']}}, te damos la bienvenida a nuestro sistema de cotización</span><br>Te saluda tu asesor(a) <b>{{$data['name_seller']}}.</b>
					</p>
					<ul>
						<li class="sucursal"><a target="_blank" href="https://maps.googleapis.com/maps/api/staticmap?center={{$data['lat']}},{{$data['lon']}}&zoom=13&scale=1&size=600x300&maptype=roadmap&key=AIzaSyBeLRu8MD1OZ68609--lImN-C_x_PRGELc&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C{{$data['lat']}},{{$data['lon']}}">{{$data['sucursal']}}</a> </li>
						<li class="email">{{$data['email']}}</li>
						<li class="telefono">{{$data['phone']}}</li>
					</ul>
					<ul class="social">
                    	<li><a target="_blank" href="{{$data['instagram']}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    	<li><a target="_blank" href="{{$data['twitter']}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    	<li><a target="_blank" href="{{$data['facebook']}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    </ul>
				</div>

			</div>
		</div>
	</div>
	<div class="cars-details">
		<div class="container">
			<div class="row quote-content">
				<div class="col-md-6 cars-selector">
					<ul>
						@foreach($quotes as $quote)
							<li><button type="button" class="btn btn-danger" data-target-id="{{$quote->patterns_id}}">{{$quote->description}}</button></li>
						@endforeach
					</ul>
				</div>
				@foreach($quotes as $quote)
				<div class="car" id="{{$quote->patterns_id}}">
					<div class="col-md-7 galery">
						<div class="colors"></div>
						<div class="vr360">
							<ul class="vrImage">
								<li style="z-index: 10; opacity: 1;"><img alt="" src="{{url('/').$pathToFilePF[$quote->patterns_id]}}"></li>
							</ul>
							<div class="control">
								<ul class="rotate">
									<li class="prev"><a href="#" onclick="return false;" class="fa fa-angle-left"></a></li>
									<li class="next"><a href="#" onclick="return false;" class="fa fa-angle-right"></a></li>
								</ul>
								<p class="indicator"></p>
								<div class="colorSelect">
									<dl class="color" style="width: 280px;">

									</dl>
								</div>
								<div class="colorName"></div>
							</div>
						</div>
						<h1>{{$quote->description}}</h1>
					</div>
					<div class="col-md-5 model-info">
						<div class="models">
							<p class="title">VERSIONES DISPONIBLES</p>
							<ul>
                                @foreach($subpatters->where('patterns_id',$quote->patterns_id) as $subpatter)
                                    <li>
                                        <img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/000.jpg"}}" alt="..." class="img-check">
                                        <span class="model-price">${{$subpatter->prize}}</span>
                                        <span class="model-description">{{$subpatter->features}}</span>
                                        <span class="model-print">documents/download/{{$quote->patterns_id}}/{{$subpatter->id}}/{{ $data['name_seller'] }}/1/{{$quote->id}}</span>
										<span class="model-download">documents/download/{{$quote->patterns_id}}/{{$subpatter->id}}/{{ $data['name_seller'] }}/2/{{$quote->id}}</span>
										<ul class="model-accesories hidden">

		                                    {{--<li>--}}
		                                        {{--<img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/000.jpg"}}" alt="..." class="img-check">--}}
		                                    {{--</li>--}}
		                                    <li>
		                                        <img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/001.jpg"}}" alt="..." class="img-check">
		                                    </li>
		                                    <li>
		                                        <img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/002.jpg"}}" alt="..." class="img-check">
		                                    </li>
		                                    <li>
		                                        <img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/003.jpg"}}" alt="..." class="img-check">
		                                    </li>
		                                    <li>
		                                        <img src="/assets/images/cars/{{$quote->patterns_id}}/sub/{{$subpatter->id."/004.jpg"}}" alt="..." class="img-check">
		                                    </li>

										</ul>
                                    </li>                                   

                                @endforeach
							</ul>

						</div>
						<div class="info">

							<p class="description">{{$subpatter->features}}</p>
							<p class="price hidden">{{$subpatter->prize}}$</p>
                            
                            {{--<a target="_blank" href="documents/download/{{$quote->patterns_id}}/{{$subpatter->id}}/{{ $data['name_seller'] }}/1/{{$quote->id}}"  class="print" style="" aria-hidden="true"><span style="display: block; padding-left: 10px;">Imprimir Catalogo</span></a>--}}

                            <a target="_blank" href="documents/download/{{$quote->patterns_id}}/{{$subpatter->id}}/{{ $data['name_seller'] }}/2/{{$quote->id}}"  class="download" style="" aria-hidden="true"><span style="display: block; padding-left: 1px;">Descargar Cotizacion</span></a>

							<ul class="accesories">
								<p class="title">ACCESORIOS</p>

							</ul>

						</div>

					</div>

				</div>
                @endforeach
			</div>
		</div>		
	</div>
</div>

<script type="text/javascript" src="assets/js/vr360.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){

		$(".cars-selector ul li button").click(function(){

			var target = $(this).attr("data-target-id");

			$(".car").hide().removeClass("active");
			$($("#"+target).fadeIn().addClass("active").find(".models ul li")[0]).trigger("click");

		});

		$($(".car")[0]).fadeIn().addClass("active");

		$(".models > ul li").click(function(){
			$(".car.active .model-info .description").hide().html($(this).find(".model-description").html()).fadeIn();
			$(".car.active .model-info .price").hide().html($(this).find(".model-price").html()).fadeIn();

			$(".car.active .model-info .print").attr("href", $(this).find(".model-print").html());
			$(".car.active .model-info .download").attr("href", $(this).find(".model-download").html());

			$(".car.active .model-info .accesories li").remove();
			$(".car.active .model-info .accesories").append($(this).find(".model-accesories li").clone());

			$(".vrImage li img").attr("src", $(this).find(".model-image").attr("src"));

			$(".models > ul li").removeClass("current-model");
			$(this).addClass("current-model");
		});
	  
		$($(".models ul li")[0]).trigger("click");
	});

	/*$(".car div.vr360").each(function(){
        var count = $('#count').val();
		console.log($(this));
		Set360VR($(this), count);
	});*/

</script>

<style type="text/css">

	.avatar {
		max-width: 60px;
		margin: 0 auto;
	}

	.no-wc-breadcrumb .site-header {
    	padding-top: 46px !important;
    }

	.btn-danger {
        color: #fff;
        background-color: #E52429;
        border-color: #E52429;
    }

	.car {
		display: none;
	}

	.top {
		margin-left: -15px;
    	margin-right: -15px;
    	background: #fff;
    	border-bottom: 1px solid red;
    	padding-top: 10px;
	}

	.top .agent-info p span {
		color: #000;
    	font-weight: bolder;
	}

	.top .agent-info .client {
		color: #000;
    	font-weight: bolder;
	}

    @media (max-width: 991px){
        .agent-info  {
            text-align: center;
            margin-bottom: 15px; 
        }
    }

	ul, ol{
		padding: 0;
    	margin: 0;
    	list-style: none;
	}

	.top .agent-info ul li {
		display: inline-block;
    	margin-right: 10px;
    	padding-left: 20px
	}

	.top .agent-info ul li.telefono {
		background: url("assets/icon/phone.png") left center no-repeat;
	}
	
	.top .agent-info ul li.sucursal {
		background: url("assets/icon/location.png") left center no-repeat;
	}

	.top .agent-info ul li.email {
		background: url("assets/icon/mail.png") left center no-repeat;
	}

	.top .contact-me {
		text-align: center;
	}

	.cars-details {
		background: #fff;
	}

	.cars-selector {
		margin: 20px 0;
	}

	.cars-selector h1 {
		color: #000;
		font-weight: bold;
	}

	.cars-details .cars-selector ul {
		margin: 0 auto;
		text-align: center;
	}

	.cars-details .cars-selector ul li{
		display: inline-block;
		margin: 5px;
	}

	.galery h1 {
		text-align: center;
		color: #000;
		font-weight: bold;
		margin-top: 5px;
		margin-top: 30px;
	}

	.images {
		position: relative;
	}

	.images ul li{
		position: absolute;
		top: 0;
		opacity: 0;
	}

	.control {
		position: absolute;
		top: 0;
		width: 100%;
		display: none;
	}

	div.vr360 {
    	height: 330px !important;
	}

	@media (max-width: 444px){
		div.vr360 {
	    	height: 230px !important;
		}
	}

	div.vr360 {
	    width: 100%;
    	height: 310px;
	    position: relative;
	    padding-top: 40px;
	    margin-top: 40px;
	}

	div.vr360 ul.vrImage {
	    /* width: 960px;
	    height: 340px; */
	    position: relative;
	    overflow: hidden;
	    cursor: pointer;
	    width: 100%;
    	height: 310px;
	}

	div.vr360 ul.vrImage li {
	    /* width: 960px;
	    height: 340px; */
	    position: absolute;
	    left: 0;
	    top: 0;
	    text-align: center;
	    opacity: 0;
	    height: 100%;
		width: 100%;
	}

	div.vr360 div.control dl.color dd.first {
    	margin-left: 0;
	}

	div.vr360 div.control dl.color dd {
	    float: left;
	    margin-left: 5px;
	}

	div.vr360 div.control dl.color dd a {
		display: inline-block;
	}

	div.vr360 div.control dl.color dd a span {
		border: 2px solid #999;
		border-radius: 50px;
	}

	div.vr360 div.control dl.color dd span.colorCode, div.vr360 div.control dl.color dd span.path {
	    display: none;
	}

	div.vr360 div.control dl.color dd span.color {
	    display: block;
	    width: 85%;
	    height: 85%;
	    background: #ffffff;
	}

	div.vr360 div.control .colorSelect {
	    width: 100%;
	    position: absolute;
	    top: 10px;
	    left: 0;
	    z-index: 998;
	}

	div.vr360 div.control dl.color {
	    overflow: hidden;
	    margin: 0 auto;
	}

	div.vr360 div.control ul.rotate li.prev {
	    left: 40px;
	}

	div.vr360 div.control ul.rotate li.next {
	    right: 40px;
	}

	div.vr360 div.control ul.rotate li {
	    top: 150px !important;
	}

	div.vr360 div.control ul.rotate li {
	    position: absolute;
	    top: 190px;
	    z-index: 100;
	}

	div.vr360 div.control ul.rotate li a {
	    position: relative;
	    font-size: 70px;
	    color: #78341a;
	    text-decoration: none!important;
	}

	div.vr360 ul li img {
		margin: 0 auto;
	}

    @media (min-width: 991px){
        .model-info {
           min-height: 575px;
            border-left: 2px solid #999;
            transform: skew(-15deg);
            box-shadow: -4px 0px 10px -3px rgba(0,0,0,.4);
            z-index: 20;
            overflow: hidden;
            margin-top: -75px;
        }

        .model-info > * {
            transform: skew(15deg);
        }

        .model-info .price {
            border-top: 1px solid #f0f0f0;
            border-bottom: 1px solid #f0f0f0;
            margin-left: -40px;
        }

        .model-info .models {
            /* margin-left: 60px; */   
        }

        .info {
        	margin-left: 130px;
        }

        .info > a {
        	display: inline-block;
        	margin: 5px;
        }

        .info p {
        	padding-top: 40px;
        }

        .social {
        	margin-left: 110px;
        }
    }	

	.model-info h1, .model-info p{
		text-align: center;
		color: #000;
		font-weight: bold;
	}

    .model-info .description{
        font-weight: bold;
        font-size: 15px;
        color: #000;
    }

	.price {
		font-size: 60px;
        color: #E52429;
	}

	.models ul {
		text-align: center;
	}

	.models ul li, .info .accesories li {
		vertical-align: top;
		display: inline-block;
		margin: 5px 5px;
		width: 85px; 
	     height: 60px; 
	   
	     /* -webkit-transform: skew(-20deg);
	     -moz-transform: skew(-20deg);
	     -ms-transform: skew(-20deg);
	     -o-transform: skew(-20deg);
	     transform: skew(-20deg); */
	     overflow: hidden;
	     cursor: pointer;
	}

	.models ul li.current-model{
		 border: 2px solid #555; 
	}

	.models ul li img, .info .accesories li img {
		height: 100%;
		width: auto;
	}

	.models .model-price, .models .model-description, .models .model-print, .models .model-download {
		display: none;
	}

    .quote-content {
        padding: 0!important;
    }

    .social {
    	font-size: 14px;
    	padding: 0;
    	margin: 0;
    }

    .social li {
    	display: inline-block;
    	padding-left: 0 !important; 
    }

    .social li a {
    	color: #000;
    }

    .social li a:hover {
    	color: #E52429;
    }

    .accesories {
    	text-align: center;
		padding-bottom: 10px;

    }

    .accesories li{
    	margin: 5px 2px!important;
    	border: 2px solid #ffffff;
    }

    .title {
    	font-size: 18px;
    	font-weight: bold;
    	padding: 10px;
    	color: #000;
    }

    .print, .download{
    	color: #000;
    	width: 45%; 
    	text-align: center; 
    	margin-top: 10px;
    	padding-top: 70px; 
    }

    .print {
    	background: url(assets/images/print.png) center center no-repeat;
    }

    .download {
    	background: url(assets/images/download.png) center center no-repeat;
    }

</style>

@endsection