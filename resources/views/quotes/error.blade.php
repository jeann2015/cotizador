@extends('layouts.front.quotation')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-5 cars-selectable">
            <h2>Error!</h2>
        </div>
    </div>
</div>

<style type="text/css">

    .cars-selectable .form-group label {
        background: #fff;
        display: block;
        padding: 20px 20px 0 20px;
    }
    
    .cars-selectable .form-group label img {
        border: none;
        margin: 0 auto;
    }

    .cars-selectable .form-group label span {
        display: block;
        padding: 10px;
        text-align: center;
        font-size: 10px;    
    }

    img.check{
        opacity:0.5;
        color:#996;       
    }

    .cars-selectable .form-group label.check span {
        color: #000;   
    }

    .personal-info .form-control {
        background: none;
        box-shadow: none;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid #ccd0d2;
    }

    .personal-info .form-control:focus {
        background: none;
    }

</style>

<script type="text/javascript">
    
        $(".img-check").on("click", function(){

            $(this).toggleClass("check").closest("label").toggleClass("check");

        });

        function changeValue(valor,checkbox){

            if(valor==0){
                $('#'+checkbox).val(1);
            }else{
                $('#'+checkbox).val(0);
            }
        }

</script>

@endsection