@extends('layouts.front.quotation')

@section('content')

<div class="container">
    <div class="row">
        {!! Form::open(array('url' => 'quotes/new')) !!}
        <div class="col-sm-12 col-lg-12 cars-selectable">

            <h2>Seleccione un modelo a cotizar</h2>
            <div class="row">
                @php
                $con=1
                @endphp
                 <ul>
            @foreach($patterndetails as $patterndetail)
                	<li><div class="form-group">                    
                        <label class="">                        	
                            <img id='imge'.{{$patterndetail->pattern_model_id}} src="{{ $pathToFile[$con] }}" alt="..." class="img-check">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <span>{{ $patterndetail->description_patterns }}</span>
                            {{ Form::checkbox( 'car'.$patterndetail->pattern_model_id, '0', null, ["onclick"=>"",'id'=>'car'.$patterndetail->pattern_model_id,'class'=>'hidden']) }}
                            {{--{{ Form::checkbox( 'car'.$patterndetail->pattern_model_id, '0', null, ["onclick"=>"changeValue(this.value,'car$patterndetail->pattern_model_id')",'id'=>'car'.$patterndetail->pattern_model_id,'class'=>'hidden']) }}--}}
                        </label>
                    </div></li>
                
                    @php
                        $con=$con+1
                    @endphp
            @endforeach

            </ul>
            </div>
        </div>

        <div class="col-sm-12 col-lg-12 personal-info">
            <h2>Complete el formulario</h2>
            <div class="card">
                <div class="container" role="alert">
                    @include('flash::message')
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-block row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            {!! Form::text('fname','',array('class' => 'form-control','id'=>'fname','required')) !!}
                        </div>                        

                        <div class="form-group">
                            <label>Apellido</label>
                            {!! Form::text('lname','',array('class' => 'form-control','id'=>'lname','required')) !!}
                        </div>

                        <div class="form-group">
                            <label>Identificación</label>
                            {!! Form::text('id_card','',array('class' => 'form-control','id'=>'id_card','required')) !!}
                        </div>
                    </div>
					
                    <div class="col-sm-6">
                        <div class="form-group">
        					<label>Teléfono</label>
        					{!! Form::text('phone','',array('class' => 'form-control','id'=>'phone','required')) !!}
                        </div>

                        <div class="form-group">
        					<label>Email</label>
        					{!! Form::text('email','',array('class' => 'form-control','id'=>'email','required')) !!}
    					</div>

                        <div class="form-group">
        					<label>Sucursal más Cercana</label>
        					{!! Form::select('offices_id', $offices, null, ['id'=>'offices_id','class'=>'form-control','required']); !!}
                        </div>

                        <div class="form-group">
                            <h3>
                                <b>
                                Desea que un Vendedor lo contacte?
                                </b>
                            </h3>
                            {{--{{ Form::checkbox( 'contacted', '0', '0', ["onclick"=>"changeValue(this.value,'contacted')",'id'=>'contacted']) }}--}}
                            {!! Form::select('contacted', ['0'=>'No','1'=>'Si'], null, ['id'=>'contacted','class'=>'form-control','required']); !!}
                        </div>

                    </div>

                    <div class="col-lg-12">
                        {!! Form::submit('Cotizar',array('class' => 'btn btn-primary','id'=>'save')) !!}
                        {!! Form::hidden('autos','0',array('class' => 'form-control','id'=>'autos')) !!}
                        {!! Form::close() !!}
                    </div>				

				</div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">

	.cars-selectable {
		max-width: 1200px;
    	margin: 0 auto;
	}

    .cars-selectable .form-group label {
        background: #fff;
        display: block;
        padding: 20px 20px 0 20px;
        position: relative;
    }
    
    .cars-selectable .form-group label img {
        border: none;
        margin: 0 auto;
    }

    .cars-selectable .form-group label span {
        display: block;
        padding: 10px;
        text-align: center;
        font-size: 10px;    
    }

    .cars-selectable ul {
    	list-style: none;
    	padding: 0 10px;
    	margin: 0;
    }

    .cars-selectable ul li {
    	vertical-align: top;
    	display: inline-block;
    	width: 200px;
    }

    img.check{
        opacity:0.5;
        color:#996;       
    }

    .fa.fa-check {
    	opacity: 0;
    	font-size: 18px;
    	position: absolute;
    	top: 20px;
    	right: 20px;
    }

    img.check + .fa.fa-check  {
    	opacity: 1;
    }

    .cars-selectable .form-group label.check span {
        color: #000;   
    }

    .personal-info .form-control {
        background: none;
        box-shadow: none;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid #ccd0d2;
    }

    .personal-info .form-control:focus {
        background: none;
    }

    @media (min-width: 768px){

    }

</style>

<script type="text/javascript">

        $(".img-check").on("click", function(){
            $(this).toggleClass("check").closest("label").toggleClass("check");
        });

        function changeValue(valor,checkbox){

            if(valor==0){

                $('#'+checkbox).val(1);


            }else{
                $('#'+checkbox).val(0);
            }

        }

        $('form').on("submit", function(){

            if($("input[type='checkbox']:checked").length <= 0){
                alert("Debes seleccionar al menos 1 vehiculo para cotizar!");
                return false;
            }

            if($("input[type='checkbox']:checked").length > 1){
                alert("Puedes seleccionar solo 1 vehiculo para cotizar!");
                return false;
            }
            return true;

        });

</script>

@endsection