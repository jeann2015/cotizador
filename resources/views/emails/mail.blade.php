<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cotización</title>
</head>
<body style="background: #f6f6f6;">
	<div class="container" style="background: #f0f0f0;
    padding: 25px;
			max-width: 890px; margin: 0 auto;">
		<dir class="row header" style="			background-color: #fff;
			margin-left: 0!important;
			margin-right: 0!important;
			margin: 0;
			border-bottom: 2px solid #efefef; padding-bottom: 20px;">
			<div class="col-sm-6" style="display: inline-block; vertical-align: top; width: 45%;">
				<a href="" class="mail-branding" style="background: url(http://isuzupanama.com.pa/wp-content/themes/copama/images/logo.png) no-repeat center #e52429;
		    background-size: 83%;
		    display: block;
		    width: 193px;
		    height: 85px;
		    float: left;
		    margin-right: 35px;
		    border-bottom-left-radius: 10px;
		    border-bottom-right-radius: 10px;
		    -webkit-box-shadow: 0px 2px 17px -1px rgba(0,0,0,.9);
		    -moz-box-shadow: 0px 2px 17px -1px rgba(0,0,0,.9);
		    box-shadow: 0px 2px 17px -1px rgba(0,0,0,.9);"></a>
			</div>
			<div class="col-sm-6" style="display: inline-block;  vertical-align: top; width: 45%;">
				<h1 style="    margin-top: 0px;
    padding-top: 32px;">Solicitud de Cotización</h1>
			</div>
		</dir>
		<dir class="row agent-info" style="			background-color: #fff;
			margin-left: 0!important;
			margin-right: 0!important;
			margin: 0;
			border-bottom: 2px solid #efefef; 			text-align: center;
    		padding: 30px 0;">
			<div class="col-sm-5" style="display: inline-block;  vertical-align: top; width: 45%;">
				<img id="imgeseller" style="max-width: 210px; height: auto; 			margin: 0 auto;
    		display: block;
    		text-align: center;" src="{{ url('/')."/".$picture_seller }}">
			</div>
			<div class="col-sm-7 info" style="display: inline-block;  vertical-align: top; width: 45%;">
				<h3>
					<span style="color: #000;
	    	font-weight: bolder;">
                        Hola {{ $fname_custom }},</span><br><b>{{$name_seller}}.</b> tu asesor(a) designado.
				</h3>
				<ul style="			padding: 0;
	    	margin: 0;
	    	list-style: none;">
					<li class="sucursal" style="			display: block;
	    	padding-left: 20px;
	    	margin: 7px; background: url("/assets/icon/location.png") left center no-repeat;"><p style="font-size: font-size: 18px">{{$sucursal}}</p></li>
					<li class="email" style="			display: block;
	    	padding-left: 20px;
	    	margin: 7px; background: url("/assets/icon/email.png") left center no-repeat;"><p style="font-size: 18px">{{$phone}}</p></li>
					<li class="telefono" style="			display: block;
	    	padding-left: 20px;
	    	margin: 7px; background: url("assets/icon/phone.png") left center no-repeat;"><p style="font-size: 18px">{{$email}}</p></li>
				</ul>
			</div>
		</dir>
		<dir class="row see-quote" style="			background-color: #fff;
			margin-left: 0!important;
			margin-right: 0!important;
			margin: 0;
			border-bottom: 2px solid #efefef; padding: 30px 0; ">
			<div class="col-sm-12 text-center" style="text-align: center; ">
				<p style="font-weight: bold;
    		font-size: 22px;">Tu cotizacion está disponible, haz click para verla.</p>
				<a href="{{ url('/quotes/details')."/".$link }}"><button class="btn btn-danger" style="    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;">Ver Cotizacion</button></a>
			</div>
		</dir>
		<dir class="row" style="			background-color: #fff;
			margin-left: 0!important;
			margin-right: 0!important;
			margin: 0 auto;
			border-bottom: 2px solid #efefef;">
			<div class="col-sm-6 map" style="padding: 20px; display: inline-block; vertical-align: top; width: 40%;">
                <img width="300" src="https://maps.googleapis.com/maps/api/staticmap?center={{$lat}},{{$lon}}&zoom=13&scale=1&size=600x300&maptype=roadmap&key=AIzaSyBeLRu8MD1OZ68609--lImN-C_x_PRGELc&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C{{$lat}},{{$lon}}" alt="Google Map of {{$lat}},{{$lon}}">
			</div>
			<div class="col-md-6 branch-info" style="padding: 20px; display: inline-block; vertical-align: top; width: 45%;">
				<h2>{{$address}}</h2>
				<p style="padding: 28px; font-size: 18px">
                    <br>
					<b>Teléfono:</b>{{$phone}}<br>
					<b>Horarios:</b>{{$schedule}}<br>
				</p>

                <div align="center" >
                    <a href="{{$facebook}}">
                    <img width="35" height="35" src="{{ url('/')."/assets/images/facebook.png" }}" alt="">
                    </a>
                    <a href="{{$twitter}}">
                    <img width="35" height="35" src="{{ url('/')."/assets/images/twitter.png" }}" alt="">
                    </a>
                    <a href="{{$instagram}}">
                    <img width="35" height="35" src="{{ url('/')."/assets/images/instagram.png" }}" alt="">
                    </a>
                </div>
			</div>
		</dir>

	</div>
</body>
</html>