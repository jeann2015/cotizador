@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Eliminar Vendedor</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="card-header">
						<a href="{{ url('sellers') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
                        {!! Form::open(array('url' => 'sellers/destroy')) !!}
                        <div class="form-group">
                            <label>Sucursal</label>
                            {!! Form::select('offices_id', $offices, $selleroffice->all(), ['id'=>'offices_id','required','class'=>'form-control multiple-select','multiple'=>'true']) !!}
                        </div>

						<div class="form-group">
							<label>Nombre</label>
							{!! Form::text('fname',$sellers->fname,array('class' => 'form-control','id'=>'fname','required')) !!}
						</div>
						<div class="form-group">
							<label>Apellido</label>
							{!! Form::text('lname',$sellers->lname,array('class' => 'form-control','id'=>'lname','required')) !!}
						</div>
						<div class="form-group">
							<label>Correo</label>
							{!! Form::text('email',$sellers->email,array('class' => 'form-control','id'=>'email','required')) !!}
						</div>
						<div class="form-group">
							<label>Telefono</label>
							{!! Form::text('phone',$sellers->phone,array('class' => 'form-control','id'=>'phone','required')) !!}
						</div>
						<div class="form-group">
							<label>Foto</label>
							<br>
							<img src="{{ url( $pathToFile[1] ) }}" height="65" width="65">
							{!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
						</div>
						{!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
						{!! Form::hidden('id',$sellers->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection