@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Nuevo Vendedor</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('sellers') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
                        {!! Form::open(array('url' => 'sellers/new','enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label>Sucursal</label>
                            {!! Form::select('offices_id[]', $offices, [], ['id'=>'offices_id','required','class'=>'form-control multiple-select','multiple'=>'true']) !!}
                        </div>

						<div class="form-group">
							<label>Nombre</label>
							{!! Form::text('fname','',array('class' => 'form-control','id'=>'fname','required')) !!}
						</div>
						<div class="form-group">
							<label>Apellido</label>
							{!! Form::text('lname','',array('class' => 'form-control','id'=>'lname','required')) !!}
						</div>
						<div class="form-group">
							<label>Correo</label>
							{!! Form::text('email','',array('class' => 'form-control','id'=>'email','required')) !!}
						</div>
						<div class="form-group">
							<label>Telefono</label>
							{!! Form::text('phone','',array('class' => 'form-control','id'=>'phone','required')) !!}
						</div>
                        <div class="form-group">
                            <label>Tipo de Vendedor</label>
                            {!! Form::select('type', array('0'=>'Vendedor','1'=>'Jefe de Vendedor'),null , ['id'=>'type','required','class'=>'form-control']) !!}
                        </div>
						<div class="form-group">
							<label>Foto</label>
							{!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
						</div>

						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection