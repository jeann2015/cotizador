@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Modificar Color</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('colors') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'colors/update')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$colors->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Color</label>
							{!! Form::text('color',$colors->color,array('class' => 'form-control','id'=>'color','required')) !!}
						</div>

						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$colors->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection