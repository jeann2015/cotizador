@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Ver Cotizacion</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('cotizaciones') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						{!! Form::open(array('url' => 'cotizaciones')) !!}

                        @foreach($quotes as $quote)
                            <div class="form-group">
                                <label>Modelo</label>
                                {!! Form::text('modelo',$quote->patterns_description,array('class' => 'form-control','id'=>'modelo','required','readonly')) !!}
                            </div>
                        @endforeach
                        <div class="form-group">
                            <label>Contactar</label>
                            <br>
                            @if($quote->contacted==1)
                                <td><span class="label label-success label-lg"> {{'Si'}}</span></td>
                            @else
                                <td><span class="label label-danger label-lg"> {{'No'}}</span></td>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nombre</label>
							{!! Form::text('name',$quote->fname_custom,array('class' => 'form-control','id'=>'name','required','readonly')) !!}
                        </div>
						<div class="form-group">
							<label>Apellido</label>
							{!! Form::text('name',$quote->lname_custom,array('class' => 'form-control','id'=>'name','required','readonly')) !!}
						</div>
						<div class="form-group">
							<label>Correo</label>
							{!! Form::text('email',$quote->email_custom,array('class' => 'form-control','id'=>'email','required','readonly')) !!}
						</div>
                        <div class="form-group">
                            <label>Phone</label>
                            {!! Form::text('phone',$quote->phone,array('class' => 'form-control','id'=>'phone','required','readonly')) !!}
                        </div>
                        <div class="form-group">
                            <label>Pasaporte/Cedula</label>
                            {!! Form::text('id_card',$quote->id_card,array('class' => 'form-control','id'=>'id_card','required','readonly')) !!}
                        </div>
						<div class="form-group">
							<label>Sucursal</label>
							{!! Form::select('offices_id', $offices, $quote->offices_id, ['id'=>'offices_id','class'=>'form-control','required','readonly']); !!}
						</div>


						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection