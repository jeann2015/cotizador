@extends('layouts.header')
@section('content')

<!-- @if(isset($notifys))

  <div class="alert alert-success" role="alert">
    <button class="close" aria-label="Close" data-dismiss="alert" type="button">
    <span aria-hidden="true">×</span>
    </button>
        @foreach ($notifys as $noti => $valor)
          @if($valor == 1)
            <strong>Well done!</strong> You successfully insert your Data.
          @endif
          @if($valor == 2)
            <strong>Well done!</strong> You successfully modified your Data.
          @endif
          @if($valor == 3)
            <strong>Well done!</strong> You successfully removed your Data,
          @endif
        @endforeach
      
  </div>
  
@endif -->

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Cotizaciones</h4>
				</div>
			</div>
		</div>
 		<div class="row">
        	<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
					  	<div class="toolbar">
				            <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
					  	</div>
					</div>
					<div class="card-block">
						<table id="GeneralQuotes" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Id</th>
						            <th>Cliente</th>
									<th>Correo</th>
									<th>Telefono</th>
									<th>Contactar?</th>
                                    <th>Vendedor</th>
                                    <th>Sucursal</th>
                                    <th>Modelo</th>
                                    <th>Fecha/Hora</th>
						            <th>Acciones</th>
								</tr>
							</thead>
			         		<tbody>
			         			@foreach ($quotes as $quote)
					              <tr>
					                  <td>{{ $quote->id }}</td>
					                  <td>{{ $quote->fname_custom. ' '.$quote->lname_custom }}</td>
                                      <td>{{ $quote->email_custom }}</td>
                                      <td>{{ $quote->phone }}</td>
                                      @if ($quote->contacted==1)
                                         <td><span class="label label-success"> {{'Si'}}</span></td>
                                      @else
                                         <td><span class="label label-danger "> {{'No'}}</span></td>
                                      @endif
                                      <td>{{ $quote->fname." ".$quote->lname }}</td>
                                      <td>{{ $quote->description_offices }}</td>
                                      <td>{{ $quote->patterns_description }}</td>
                                      <td>{{ Carbon\Carbon::parse($quote->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
					                  <td>
			                  			<div class="tabledit-toolbar btn-toolbar">
			                  				<div class="btn-group btn-group-sm">
							                    @foreach ($user_access as $user_acces)
							                      @if($user_acces->modifys == 1) 
							                        <a href="cotizaciones/view/{{ $quote->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
							                        	<span class="icofont icofont-search-alt-2"></span>
							                        </a>
							                      @endif
							                    @endforeach
						                    </div>
			                  			</div>
			                  	     </td>
					              </tr>
					            @endforeach
			         		</tbody>		             	
	  					</table>
					</div>
  				</div>
  			</div>
  		</div>
  	</div>
</div>  
@endsection