@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Eliminar Sub-Modelo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
                        @foreach($patters_details as $pattern)
						    <a href="{{ url('patterns/edit/'.$pattern->pattern_model_id) }}" class="btn btn-default" role="button">Back </a>
                        @endforeach
					</div>

					<div class="card-block">
						{!! Form::open(array('url' => 'patterns/destroysub','enctype'=>'multipart/form-data')) !!}

                        @foreach($patterns as $pattern)
                            <div class="form-group">
                                <label>Marca</label>
                                {!! Form::text('marca', $pattern->description_trade_marks ,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Modelo</label>
                                {!! Form::text('modelo', $pattern->description,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                        @endforeach

                        @foreach($patters_details as $patters_detail)
							<div class="form-group">
								<label>Estado</label>
								{!! Form::select('status', ['1'=>'Activo','0'=>'No Activo'], $patters_detail->status, ['id'=>'status','class'=>'form-control','required']) !!}
							</div>

							<div class="form-group">
                                <label>Color</label>
                                {!! Form::select('colors_id', $colors, $patters_detail->colors_id, ['id'=>'colors_id','class'=>'form-control','required']) !!}
                            </div>

                            <div class="form-group">
                                <label>Caracteristicas</label>
                                {!! Form::text('features',$patters_detail->features,array('class' => 'form-control','id'=>'features','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Precio</label>
                                {!! Form::text('prize',$patters_detail->prize,array('class' => 'form-control','id'=>'prize','required')) !!}
                            </div>
						@endforeach

                        <div class="form-group">
                            <label>Foto Principal</label>
                            <br>
							@if(isset($pathToFile) && $pathToFile<>"")
								@foreach($pathToFile as $pathToFil)
									<img src="{{ url( $pathToFil ) }}" height="65" width="65">
								@endforeach
							@endif
                        </div>
						{!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
                        {!! Form::hidden('patterns_id',$patters_detail->pattern_model_id,array('id'=>'patterns_id')) !!}
						{!! Form::hidden('id',$patters_detail->id_details,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection