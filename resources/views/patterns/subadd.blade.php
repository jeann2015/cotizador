@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Nuevo Sub-Modelo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
                        @foreach($patterns as $pattern)
						    <a href="{{ url('patterns/edit/'.$pattern->id) }}" class="btn btn-default" role="button">Back </a>
                        @endforeach
					</div>

					<div class="card-block">
						{!! Form::open(array('url' => 'patterns/newsub','enctype'=>'multipart/form-data')) !!}

                        @foreach($patterns as $pattern)
                            <div class="form-group">
                                <label>Marca</label>
                                {!! Form::text('marca', $pattern->description_trade_marks ,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Modelo</label>
                                {!! Form::text('modelo', $pattern->description,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                        @endforeach

                            <div class="form-group">
                                <label>Color</label>
                                {!! Form::select('colors_id', $colors, null, ['id'=>'colors_id','class'=>'form-control','required']) !!}
                            </div>

                            <div class="form-group">
                                <label>Caracteristicas</label>
                                {!! Form::text('features','',array('class' => 'form-control','id'=>'features','required')) !!}
                            </div>
							<div class="form-group">
								<label>Precio</label>
								{!! Form::text('prize','',array('class' => 'form-control','id'=>'prize','required')) !!}
							</div>
                            <div class="form-group">
                                <label>Fotos</label>
                                {!! Form::file('images1[]', ['id'=>'images1[]','multiple'=>'multiple','class'=>'form-control']) !!}
                            </div>
							<div class="form-group">
								<label>PDF</label>
								{!! Form::file('files', ['id'=>'files','class'=>'form-control','required']) !!}
							</div>


						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')) !!}
                        {!! Form::hidden('patterns_id',$pattern->id,array('patterns_id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection