@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Nuevo Modelo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('patterns') }}" class="btn btn-default" role="button">Back </a>
					</div>

					<div class="card-block">
						{!! Form::open(array('url' => 'patterns/new','enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label>Marca</label>
                            {!! Form::select('trade_marks_id', $trademarks, null, ['id'=>'trade_marks_id','class'=>'form-control','required']) !!}
                        </div>


						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
						</div>

						<div class="form-group">
							<label>Foto Principal</label>
							{!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
						</div>

						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection