@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Modificar Modelo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('patterns') }}" class="btn btn-default" role="button">Back </a>
					</div>

					<div class="card-block">
						{!! Form::open(array('url' => 'patterns/update','enctype'=>'multipart/form-data')) !!}
						@foreach($patterns as $pattern)
							<div class="form-group">
								<label>Marca</label>
								{!! Form::select('trade_marks_id', $trademarks, $pattern->trade_marks_id, ['id'=>'trade_marks_id','class'=>'form-control','required']) !!}
							</div>


							<div class="form-group">
								<label>Descripción</label>
								{!! Form::text('description',$pattern->description,array('class' => 'form-control','id'=>'description','required')) !!}
							</div>
                            <div class="form-group">
                                <label>Foto Principal</label>
                                <br>
                                @if(count($pathToFile[1])>=1 && $pathToFile[1]<>"")
                                    <img src="{{ url( $pathToFile[1] ) }}" height="65" width="65">
                                @endif
                                {!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
                            </div>
						@endforeach
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$pattern->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>


	</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-block">
                    <div class="toolbar">
                        <a href="{{ url('patterns/addsub/'.$pattern->id) }}" class="btn btn-default" role="button">Add Sub-Model </a>
                    </div>
                    <br>
                    <table id="General" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Sub-Modelos Caracteristica</th>
                            <th>Precio</th>
                            <th>Color</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($patters_details as $patters_detail)
                            <tr>
                                <th>{{$patters_detail->id_details}}</th>
                                <th>{{$patters_detail->features}}</th>

                                <th>{{number_format($patters_detail->prize,2)}}</th>
                                <th>{{$patters_details_images->where('pattern_model_id',$pattern->id)->pluck('description_color')[0]}}</th>
                                @if($patters_detail->status==1)
                                    <td><span class="label label-success"> {{'Activo'}}</span></td>
                                @else
                                    <td><span class="label label-danger"> {{'No Activo'}}</span></td>
                                @endif
                                <th>
                                    <a href="patterns/editsub/{{ $patters_detail->pattern_model_id }}/{{ $patters_detail->id_details }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                        <span class="icofont icofont-ui-edit"></span>
                                    </a>
                                    <a href="patterns/deletesub/{{ $patters_detail->pattern_model_id }}/{{ $patters_detail->id_details }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                        <span class="icofont icofont-ui-delete"></span>
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection