@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Modificar Sub-Modelo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
                        @foreach($patters_details as $pattern)
						    <a href="{{ url('patterns/edit/'.$pattern->pattern_model_id) }}" class="btn btn-default" role="button">Back </a>
                        @endforeach
					</div>

					<div class="card-block">
						{!! Form::open(array('url' => 'patterns/updatesub','enctype'=>'multipart/form-data')) !!}

                        @foreach($patterns as $pattern)
                            <div class="form-group">
                                <label>Marca</label>
                                {!! Form::text('marca', $pattern->description_trade_marks ,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Modelo</label>
                                {!! Form::text('modelo', $pattern->description,array('class' => 'form-control','id'=>'marca','required','readonly')) !!}
                            </div>
                        @endforeach
						<div class="form-group">
							<label>Color</label>
							{!! Form::select('colors_id', $colors, $patters_details_images->where('pattern_model_id',$pattern->id)->pluck('colors_id')[0], ['id'=>'colors_id','class'=>'form-control','required']) !!}
						</div>
                        @foreach($patters_details as $patters_detail)
							<div class="form-group">
								<label>Estado</label>
								{!! Form::select('status', ['1'=>'Activo','0'=>'No Activo'], $patters_detail->status, ['id'=>'status','class'=>'form-control','required']) !!}
							</div>

                            <div class="form-group">
                                <label>Caracteristicas</label>
                                {!! Form::text('features',$patters_detail->features,array('class' => 'form-control','id'=>'features','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Precio</label>
                                {!! Form::text('prize',$patters_detail->prize,array('class' => 'form-control','id'=>'prize','required')) !!}
                            </div>
						@endforeach

                        <div class="form-group">
                            <label>Foto Principal</label>
                            <br>
                            @if(isset($pathToFile) && $pathToFile<>"")
                                @foreach($pathToFile as $pathToFil)
                                    <img src="{{ url( $pathToFil ) }}" height="65" width="65">
                                @endforeach
                            @endif
                            {!! Form::file('images1[]', ['id'=>'images1[]','multiple'=>'multiple','class'=>'form-control']) !!}
                        </div>

						<div class="form-group">
							<label>PDF</label>
							{!! Form::file('files', ['id'=>'files','class'=>'form-control']) !!}
						</div>



						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
                        {!! Form::hidden('patterns_id',$patters_detail->pattern_model_id,array('id'=>'patterns_id')) !!}
						{!! Form::hidden('id',$patters_detail->id_details,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection