@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Modificar Sucursal</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('offices') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						{!! Form::open(array('url' => 'offices/update')) !!}

                        <div class="form-group">
                            <label>Marca</label>
                            {!! Form::select('trade_marks_id[]', $trademarks, $officetraders->all(), ['id'=>'trade_marks_id','required','class'=>'form-control multiple-select','multiple'=>'true']) !!}
                        </div>
						<div class="form-group">
							<label>Nombre</label>
							{!! Form::text('name',$offices->name,array('class' => 'form-control','id'=>'name','required')) !!}
						</div>
						<div class="form-group">
							<label>Direccion</label>
							{!! Form::text('address',$offices->address,array('class' => 'form-control','id'=>'address','required')) !!}
						</div>
						<div class="form-group">
							<label>Horario</label>
							{!! Form::textarea('schedule',$offices->schedule,array('class' => 'form-control','id'=>'schedule','required')) !!}
						</div>

                        <div class="form-group">
                            <label>Telefono</label>
                            {!! Form::text('phone',$offices->phone,array('class' => 'form-control','id'=>'phone','required')) !!}
                        </div>

                        <div class="form-group">
                            <label>latitud</label>
                            {!! Form::text('lat',$offices->lat,array('class' => 'form-control','id'=>'lat','required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Longitud</label>
                            {!! Form::text('lon',$offices->lon,array('class' => 'form-control','id'=>'lon','required')) !!}
                        </div>

						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$offices->id,array('id'=>'id')) !!}

						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection