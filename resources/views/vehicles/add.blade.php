@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Nuevo Vehiculo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('vehicles') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
                        {!! Form::open(array('url' => 'vehicles/new','enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label>Marca</label>
                            {!! Form::select('trade_marks_id', $trademarks, [], ['id'=>'trade_marks_id','required','class'=>'form-control','onchange'=>'search_patterns(this.value)']) !!}
                        </div>
						<div class="form-group">
							<label>Modelo</label>
							{!! Form::select('patterns_id', [], [], ['id'=>'patterns_id','required','class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							<label>Cabina</label>
							{!! Form::select('cabins_id', $cabins, [], ['id'=>'cabins_id','required','class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							<label>Traccion</label>
							{!! Form::select('tractions_id', $tractions, [], ['id'=>'tractions_id','required','class'=>'form-control']) !!}
						</div>

						<div class="form-group">
							<label>Descripcion</label>
							{!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
                        <div class="form-group">
                            <label>Foto 1</label>
                            {!! Form::file('images1', ['id'=>'images1','class'=>'form-control','submit']) !!}
                        </div>
                        <div class="form-group">
                            <label>Foto 2</label>
                            {!! Form::file('images2', ['id'=>'images2','class'=>'form-control','submit']) !!}
                        </div>
                        <div class="form-group">
                            <label>Foto 3</label>
                            {!! Form::file('images3', ['id'=>'images3','class'=>'form-control','submit']) !!}
                        </div>
                        <div class="form-group">
                            <label>Foto 4</label>
                            {!! Form::file('images4', ['id'=>'images4','class'=>'form-control','submit']) !!}
                        </div>

						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection