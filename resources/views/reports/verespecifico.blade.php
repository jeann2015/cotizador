@extends('layouts.headerreports')
@section('content')

    @foreach ($eventos as $evento)
    {!! Form::open(array('url' => 'respecifico/ver/report/graph/')) !!}
    <div class="content-wrapper">
        <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="main-header">
                <h4>Reporte Especifico</h4>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <tr>
            <td colspan="2">
                <a href="{{ url('respecifico') }}" class="btn btn-default" role="button">Back </a>
            </td>
        </tr>
        <tr class="info">
            <td colspan="2">
                Datos del Evento
            </td>
        </tr>

        <tr>
            <td>{{ $evento->tipo }}:</td>
            <td colspan="2">
                {{ $evento->tipo }}
            </td>
        </tr>
        <tr>
            <td>Facilitador:</td>
            <td colspan="2">
                {{ $evento->facilitador }}
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td colspan="2">
                {{ $evento->fecha_evento }}
            </td>
        </tr>
        <tr>
            <td>Organización:</td>
            <td colspan="2">
                {{ $evento->organizacion }}
            </td>
        </tr>
        <tr>
            <td>Lugar:</td>
            <td colspan="2">
                {{ $evento->lugar }}
            </td>
        </tr>

        <tr>
            <td>Asistentes:</td>
            <td colspan="2">
                {{ $evento->asistentes }}
            </td>
        </tr>


        <tr>
            <td>Participantes:</td>
            <td colspan="2">
                {{ $evento->participantes }}
            </td>
        </tr>


        <tr>
            <td colspan="2">
                {!! Form::submit('Lanzar!',array('class' => 'btn btn-primary','id'=>'lanzar')) !!}
                {!! Form::hidden('id',$evento->id,array('id'=>'id')) !!}
            </td>
        </tr>

        <tr class="success">
            <td colspan="2">Opciones</td>
        </tr>
        <tr class="success">
            <td colspan="2">
                {!! Form::select('opciones',['1'=>'Ver en Pantalla','2'=>'Descargar PDF','3'=>'Enviar por Correo a un Funionario','4'=>'Enviar por Correo a un Cliente'],'', ['class'=>'form-control','required']) !!}
            </td>
        </tr>



    </table>
        </div>
    </div>
    {!! Form::close() !!}
    @endforeach
@endsection