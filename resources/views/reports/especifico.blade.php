@extends('layouts.header')

@section('content')


    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Reporte Cotizaciones</h4>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'reports/search')) !!}
                            <div class="form-group">
                                <label>Sucursales</label>
                                @if(isset($values['offices_id']) )
                                    {!! Form::select('offices_id', $offices, $values['offices_id'], ['id'=>'offices_id','class'=>'form-control','onchange'=>'search_sellers_all(this.value)','required']) !!}
                                @else
                                    {!! Form::select('offices_id', $offices, null, ['id'=>'offices_id','class'=>'form-control','onchange'=>'search_sellers_all(this.value)','required']) !!}
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Venedores:</label>
                                <br>
                                @if(isset($values['sellers_id']) )
                                    {!! Form::select('sellers_id', $sellers, $values['sellers_id'], ['id'=>'sellers_id','class'=>'form-control','required']) !!}
                                @else
                                    {!! Form::select('sellers_id', ['0'=>'Todos Los Vendedores'], [], ['id'=>'sellers_id','class'=>'form-control','required']) !!}
                                @endif
                            </div>

                            <div class="form-group">
                                <table class="table">
                                    <tr>
                                        <td>Desde</td>
                                        <td>{!! Form::text('desde', $values['desde'] ,array('class' => 'form-control','id'=>'desde','required')) !!}</td>
                                        <td>Hasta</td>
                                        <td>{!! Form::text('hasta',$values['hasta'],array('class' => 'form-control','id'=>'hasta','required')) !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Buscar!',array('class' => 'btn btn-primary','id'=>'buscar')); !!}
                            </div>
                        </div>
                        @if(isset($quotes))
                            <table id="GeneralQuotes" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cliente</th>
                                    <th>Correo</th>
                                    <th>Telefono</th>
                                    <th>Contactar?</th>
                                    <th>Vendedor</th>
                                    <th>Sucursal</th>
                                    <th>Modelo</th>
                                    <th>Fecha/Hora</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($quotes as $quote)
                                    <tr>
                                        <td>{{ $quote->id }}</td>
                                        <td>{{ $quote->fname_custom. ' '.$quote->lname_custom }}</td>
                                        <td>{{ $quote->email_custom }}</td>
                                        <td>{{ $quote->phone }}</td>
                                        @if ($quote->contacted==1)
                                            <td><span class="label label-success"> {{'Si'}}</span></td>
                                        @else
                                            <td><span class="label label-danger "> {{'No'}}</span></td>
                                        @endif
                                        <td>{{ $quote->fname." ".$quote->lname }}</td>
                                        <td>{{ $quote->description_offices }}</td>
                                        <td>{{ $quote->patterns_description }}</td>
                                        <td>{{ Carbon\Carbon::parse($quote->created_at)->format('l jS \\of F Y h:i:s A') }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection


