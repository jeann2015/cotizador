<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table="vehicles";

    protected $fillable = [
        'description',
        'trade_marks_id',
        'patterns_id',
        'cabins_id',
        'tractions_id'
    ];

    /**
     * @param int $id
     * @return mixed
     */
    public function get_vehicles($id=0)
    {
        $vehicles = Vehicle::join('trade_marks', function ($join) use ($id) {
            if ($id<>0) {
                $join->on('trade_marks.id', '=', 'vehicles.trade_marks_id')
                        ->where('vehicles.id', '=', $id);
            } else {
                $join->on('trade_marks.id', '=', 'vehicles.trade_marks_id');
            }
        })
            ->join('patterns', 'patterns.id', 'vehicles.patterns_id')
            ->join('cabins', 'cabins.id', 'vehicles.cabins_id')
            ->join('tractions', 'tractions.id', 'vehicles.tractions_id')
            ->select('vehicles.*',
                'patterns.description as description_patterns',
                'cabins.description as description_cabins',
                'trade_marks.description as description_trade_marks',
                'tractions.description as description_tractions'
            )->get();

        return $vehicles;
    }
}
