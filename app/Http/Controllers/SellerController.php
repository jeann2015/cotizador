<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Modules;
use App\Office;
use App\Seller;
use App\SellerOffice;
use App\User;
use App\UserSeller;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $seller = new Seller;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $sellers = $seller->get_sellers_offices();

        return view('sellers.index', compact('sellers', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $offices = Office::select('name', 'id')->pluck('name', 'id');
        return view('sellers.add', compact('offices', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'email' => 'required|unique:users',
            'offices_id'=>'required|numeric|exists:offices,id',
            'images1' => 'required:image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images1 = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);
        }

        $sellers = Seller::create([
            'fname'=>$request->fname,
            'lname'=>$request->lname,
            'email'=>$request->email,
            'images'=>$images1,
            'status'=>1
        ]);

        foreach ($request->offices_id as $offices_ids) {
            SellerOffice::create([
                'offices_id' => $offices_ids,
                'sellers_id' => $sellers->id
            ]);
        }

        $user = new User;
        $user->news_user_sellers($request->fname." ".$request->lname, $request->email, 2, $sellers->id);

        $audits = new Audits;
        $audits->save_audits('Add new Sellers:'.$sellers->id." - ".$request->fname." ".$request->lname);
        return redirect('sellers');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $sellers = Seller::find($request->id);

        if ($sellers->images <> "") {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile[1] = $link . '.jpg';
            \Image::make($sellers->images, 100, 100)->save($pathToFile[1]);
        } else {
            $pathToFile[1] = "";
        }

        $offices = Office::select('name', 'id')->pluck('name', 'id');
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $selleroffice = SellerOffice::where('sellers_id', $request->id)->pluck('offices_id');

        return view('sellers.mod', compact('pathToFile', 'selleroffice', 'offices', 'sellers', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'email' => 'required',
//            'offices_id'=>'required|numeric|exists:offices,id',
            'id'=>'required|numeric|exists:sellers,id'
        ]);

        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images1 = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);
        } else {
            $images1="";
        }

        if ($images1<>"") {
            Seller::where('id', $request->id)->update([
                'fname'=>$request->fname,
                'lname'=>$request->lname,
                'email'=>$request->email,
                'images'=>$images1,
                'status'=>$request->status,
                'phone'=>$request->phone,
                'type'=>$request->type
            ]);
        } else {
            Seller::where('id', $request->id)->update([
                'fname'=>$request->fname,
                'lname'=>$request->lname,
                'email'=>$request->email,
                'status'=>$request->status,
                'type'=>$request->type
            ]);
        }

        SellerOffice::where('sellers_id', $request->id)->delete();

        foreach ($request->offices_id as $offices_ids) {
            SellerOffice::create([
                'offices_id' => $offices_ids,
                'sellers_id' => $request->id
            ]);
        }

        $audits = new Audits;

        $audits->save_audits('Modify Sellers:'.$request->id." - ".$request->description);
        return redirect('sellers');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $sellers = Seller::find($request->id);
        $offices = Office::select('name', 'id')->pluck('name', 'id');
        $selleroffice = SellerOffice::where('sellers_id', $request->id)->pluck('offices_id');
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        if ($sellers->images <> "") {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile[1] = $link . '.jpg';
            \Image::make($sellers->images, 100, 100)->save($pathToFile[1]);
        } else {
            $pathToFile[1] = "";
        }

        return view('sellers.del', compact('pathToFile', 'selleroffice', 'offices', 'sellers', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'email' => 'required',
            'offices_id'=>'required|numeric|exists:offices,id',
            'id'=>'required|numeric|exists:sellers,id'
        ]);

        SellerOffice::where('sellers_id', $request->id)->delete();
        Seller::where('id', $request->id)->delete();
        $audits = new Audits;

        $audits->save_audits('Delete Sellers:'.$request->id." - ".$request->fname."".$request->lname);
        return redirect('sellers');
    }
}
