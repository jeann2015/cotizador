<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Grupos;
use App\GruposUsers;
use App\Modules;
use App\GruposModules;
use App\Audits;
use App\User;
use App\Access;

use Illuminate\Support\Collection as Collection;

class GruposController extends Controller
{
    /**
     * GruposController constructor.
     */
  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $grupos = Grupos::all();


        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('grupos.index', compact('grupos', 'user_access', 'module_principals', 'module_menus'));
    }

    public function add(Request $request)
    {
        $module = \DB::table('modules')
    ->select('modules.id',
      'modules.description',
      'modules.order',
      'modules.id_father',
      'modules.url',
      'modules.messages',
      'modules.status',
      'modules.visible')->get();

        $modules = Collection::make($module);



        foreach ($modules as $module) {
            $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,
            'checkedview'.$module->id=>'nochecked',
            'onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')',
            'valueView'.$module->id=>'0');

            $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,
            'checkedsave'.$module->id=>'nochecked',
            'onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')',
            'valueSave'.$module->id=>'0');

            $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,
            'checkedmodify'.$module->id=>'nochecked',
            'onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')',
            'valueModify'.$module->id=>'0');

            $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,
            'checkeddelete'.$module->id=>'nochecked',
            'onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')',
            'valueDelete'.$module->id=>'0');
        }

        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('grupos.add', compact('modules', 'arr_view_arr', 'arr_save_arr',
          'arr_modify_arr', 'arr_delete_arr', 'module_principals', 'module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;
        $grupos = new Grupos;
        $modules = Modules::all();
        $grupos->description = $request->description;
        $grupos->save();

        $count_module = $request->count_mod;

        foreach ($modules as $module) {
            $gruposmodules = new GruposModules;

            $view='view'.$module->id;
            $view_value = $request->$view;
            if ($view_value=="") {
                $view_value="0";
            }

            $save='save'.$module->id;
            $save_value = $request->$save;
            if ($save_value=="") {
                $save_value="0";
            }

            $modify='modify'.$module->id;
            $modify_value = $request->$modify;
            if ($modify_value=="") {
                $modify_value="0";
            }

            $delete='delete'.$module->id;
            $delete_value = $request->$delete;
            if ($delete_value=="") {
                $delete_value="0";
            }

            if ($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"") {
                $gruposmodules->id_grupos=$grupos->id;
                $gruposmodules->id_modules=$module->id;
                $gruposmodules->views=$view_value;
                $gruposmodules->inserts=$save_value;
                $gruposmodules->modifys=$modify_value;
                $gruposmodules->deletes=$delete_value;
                $gruposmodules->save();
            }
        }

        $audits->save_audits('Add new Grupos:'.$grupos->id." - ".$request->description);
        return redirect('grupos');
    }


    public function edit(Request $request)
    {
        $iduser = \Auth::id();
        $idgrupobuscar = $request->id;
        $grupos = Grupos::find($idgrupobuscar);

        $module = \DB::table('modules')
     ->select('modules.id',
       'modules.description',
       'modules.order',
       'modules.id_father',
       'modules.url',
       'modules.messages',
       'modules.status',
       'modules.visible',
       'grupos_modules.views',
       'grupos_modules.inserts',
       'grupos_modules.modifys',
       'grupos_modules.deletes')
     ->leftjoin('grupos_modules', function ($join) use ($idgrupobuscar) {
         $join->on('grupos_modules.id_modules', '=', 'modules.id')->where('grupos_modules.id_grupos', '=', $idgrupobuscar);
     })->get();

        $modules = Collection::make($module);

        foreach ($modules as $module) {
            if ($module->views == 1) {
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
            } else {
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
            }

            if ($module->inserts == 1) {
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
            } else {
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
            }

            if ($module->modifys == 1) {
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
            } else {
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
            }

            if ($module->deletes == 1) {
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
            } else {
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
            }
        }

        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('grupos.mod', compact('modules', 'grupos',
           'arr_view_arr', 'arr_save_arr', 'arr_modify_arr', 'arr_delete_arr', 'grupos', 'module_principals', 'module_menus'));
    }


    public function update(Request $request)
    {
        $audits = new Audits;
        $grupos = Grupos::find($request->id);
        $grupos->description = $request->description;
        $grupos->save();

        $modules = Modules::all();

        foreach ($modules as $module) {
            $view='view'.$module->id;
            $view_value = $request->$view;
            if ($view_value=="") {
                $view_value="0";
            }

            $save='save'.$module->id;
            $save_value = $request->$save;
            if ($save_value=="") {
                $save_value="0";
            }

            $modify='modify'.$module->id;
            $modify_value = $request->$modify;
            if ($modify_value=="") {
                $modify_value="0";
            }

            $delete='delete'.$module->id;
            $delete_value = $request->$delete;
            if ($delete_value=="") {
                $delete_value="0";
            }

            $grupos_module = \DB::table('grupos_modules')
           ->where('grupos_modules.id_modules', '=', $module->id)
           ->where('grupos_modules.id_grupos', '=', $request->id)->count();



            if ($grupos_module>0) {
                if ($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"") {
                    $var_datetime = date('Y-m-d H:s:i');
                    $gruposmodules = \DB::update("update grupos_modules set views =".$view_value.",inserts=".$save_value.",modifys=".$modify_value.",
                   deletes=".$delete_value.",updated_at = '".$var_datetime."'
                   where id_modules = ".$module->id." and id_grupos = ".$request->id);

                    $users = User::all();
                    foreach ($users as $user) {
                        $access = \DB::update("update access set views =".$view_value.",inserts=".$save_value.",modifys=".$modify_value.",
                     deletes=".$delete_value.",updated_at = '".$var_datetime."'
                     where id_module = ".$module->id." and id_user = ".$user->id);
                    }
                }
            } else {
                $gruposmodules = new GruposModules;
                $gruposmodules->id_grupos=$request->id;
                $gruposmodules->id_modules=$module->id;
                $gruposmodules->views=$view_value;
                $gruposmodules->inserts=$save_value;
                $gruposmodules->modifys=$modify_value;
                $gruposmodules->deletes=$delete_value;
                $gruposmodules->save();

                $users = User::all();
                foreach ($users as $user) {
                    $access = new Access;
                    $access->id_user=$user->id;
                    $access->id_module=$module->id;
                    $access->views=0;
                    $access->inserts=0;
                    $access->modifys=0;
                    $access->deletes=0;
                    $access->status=0;
                    $access->save();
                }
            }
        }

        $audits->save_audits('Modify Grupo:'.$request->id." - ".$request->description);
        return redirect('grupos');
    }


    public function delete(Request $request)
    {
        $iduser = \Auth::id();
        $idgrupobuscar = $request->id;
        $grupos = Grupos::find($idgrupobuscar);

        $module = \DB::table('modules')
     ->select('modules.id',
       'modules.description',
       'modules.order',
       'modules.id_father',
       'modules.url',
       'modules.messages',
       'modules.status',
       'modules.visible',
       'grupos_modules.views',
       'grupos_modules.inserts',
       'grupos_modules.modifys',
       'grupos_modules.deletes')
     ->leftjoin('grupos_modules', function ($join) use ($idgrupobuscar) {
         $join->on('grupos_modules.id_modules', '=', 'modules.id')->where('grupos_modules.id_grupos', '=', $idgrupobuscar);
     })->get();

        $modules = Collection::make($module);

        foreach ($modules as $module) {
            if ($module->views == 1) {
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
            } else {
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
            }

            if ($module->inserts == 1) {
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
            } else {
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
            }

            if ($module->modifys == 1) {
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
            } else {
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
            }

            if ($module->deletes == 1) {
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
            } else {
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
            }
        }

        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('grupos.del', compact('modules', 'grupos', 'arr_view_arr',
           'arr_save_arr', 'arr_modify_arr', 'arr_delete_arr', 'grupos', 'module_principals', 'module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;
        $idgrupobuscar = $request->id;

        $grupos = Grupos::find($request->id);
        GruposModules::where('id_grupos', $idgrupobuscar)->delete();

        $audits->save_audits('Delete Grupo:'.$request->id." - ".$request->description);
        $grupos->delete();
      
        return redirect('grupos');
    }
}
