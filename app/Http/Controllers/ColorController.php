<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Color;
use App\Modules;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $colors = Color::all();
        return view('colors.index', compact('colors', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('colors.add', compact('module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'color' => 'required|min:3'
        ]);

        $colors = Color::create([
            'description'=>$request->description,
            'color'=>$request->color
        ]);

        $audits = new Audits;

        $audits->save_audits('Add new Color:'.$colors->id." - ".$request->description);
        return redirect('colors');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $colors = Color::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('colors.mod', compact('colors', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'color' => 'required|min:3'
        ]);

        Color::where('id', $request->id)->update([
            'description'=>$request->description,
            'color'=>$request->color
        ]);

        $audits = new Audits;

        $audits->save_audits('Modify Color:'.$request->id." - ".$request->description);
        return redirect('colors');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $colors = Color::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('colors.del', compact('colors', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'color' => 'required|min:3'
        ]);

        Color::find($request->id)->delete();
        $audits = new Audits;

        $audits->save_audits('Delete Color:'.$request->id." - ".$request->description);
        return redirect('colors');
    }
}
