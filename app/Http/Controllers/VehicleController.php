<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Cabin;
use App\Modules;
use App\Pattern;
use App\Traction;
use App\TradeMark;
use App\Vehicle;
use App\VehicleImagen;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $vehicle = new Vehicle;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $vehicles = $vehicle->get_vehicles();
        return view('vehicles.index', compact('vehicles', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());

        $cabin = Cabin::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Cabina');
        $cabins = array_sort_recursive($cabin->toArray());

        $traction = Traction::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Traccion');
        $tractions = array_sort_recursive($traction->toArray());

        return view('vehicles.add', compact('tractions', 'cabins', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'trade_marks_id' =>'required|numeric|exists:trade_marks,id',
            'patterns_id' =>'required|numeric|exists:patterns,id',
            'cabins_id' =>'required|numeric|exists:cabins,id',
            'tractions_id' =>'required|numeric|exists:tractions,id',
            'images1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images4' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $vehicles = Vehicle::create([
            'description'=>$request->description,
            'trade_marks_id'=>$request->trade_marks_id,
            'patterns_id'=>$request->patterns_id,
            'cabins_id'=>$request->cabins_id,
            'tractions_id'=>$request->tractions_id
        ]);

        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $vehicles->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images2')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images2')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $vehicles->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images3')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images3')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $vehicles->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images4')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images4')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $vehicles->id,
                'images' => $images,
            ]);
        }

        $audits = new Audits;

        $audits->save_audits('Add new Vehicles:'.$vehicles->id." - ".$request->description);
        return redirect('vehicles');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $vehicle = new Vehicle;

        $vehicles = $vehicle->get_vehicles($request->id);

        foreach ($vehicles as $vehicle) {
            $trademark_id = $vehicle->patterns_id;
        }
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());

        $patterns = Pattern::select('description', 'id')->where('id', $trademark_id)->pluck('description', 'id');

        $cabin = Cabin::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Cabina');
        $cabins = array_sort_recursive($cabin->toArray());

        $traction = Traction::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Traccion');
        $tractions = array_sort_recursive($traction->toArray());

        return view('vehicles.mod', compact('patterns', 'vehicles', 'tractions', 'cabins', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'trade_marks_id' =>'required|numeric|exists:trade_marks,id',
            'patterns_id' =>'required|numeric|exists:patterns,id',
            'cabins_id' =>'required|numeric|exists:cabins,id',
            'tractions_id' =>'required|numeric|exists:tractions,id',
            'images1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'images4' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        Vehicle::where('id', $request->id)->update([
            'description'=>$request->description,
            'trade_marks_id'=>$request->trade_marks_id,
            'patterns_id'=>$request->patterns_id,
            'cabins_id'=>$request->cabins_id,
            'tractions_id'=>$request->tractions_id
        ]);

        VehicleImagen::where('vehicles_id', $request->id)->delete();

        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $request->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images2')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images2')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $request->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images3')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images3')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $request->id,
                'images' => $images,
            ]);
        }
        if ($request->file('images4')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images = \Image::make($request->file('images4')->getRealPath())
                ->save($pathToFile);

            VehicleImagen::create([
                'vehicles_id' => $request->id,
                'images' => $images,
            ]);
        }

        $audits = new Audits;
        $audits->save_audits('Modify Vehicles:'.$request->id." - ".$request->description);
        return redirect('vehicles');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $vehicle = new Vehicle;

        $vehicles = $vehicle->get_vehicles($request->id);

        foreach ($vehicles as $vehicle) {
            $trademark_id = $vehicle->patterns_id;
        }
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());

        $patterns = Pattern::select('description', 'id')->where('id', $trademark_id)->pluck('description', 'id');

        $cabin = Cabin::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Cabina');
        $cabins = array_sort_recursive($cabin->toArray());

        $traction = Traction::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Traccion');
        $tractions = array_sort_recursive($traction->toArray());

        return view('vehicles.del', compact('patterns', 'vehicles', 'tractions', 'cabins', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'trade_marks_id' =>'required|numeric|exists:trade_marks,id',
            'patterns_id' =>'required|numeric|exists:patterns,id',
            'cabins_id' =>'required|numeric|exists:cabins,id',
            'tractions_id' =>'required|numeric|exists:tractions,id'
        ]);

        VehicleImagen::where('vehicles_id', $request->id)->delete();

        Vehicle::find($request->id)->delete();

        $audits = new Audits;
        $audits->save_audits('Delete Vehicles:'.$request->id." - ".$request->description);
        return redirect('vehicles');
    }
}
