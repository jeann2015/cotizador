<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Modules;
use App\Traction;
use Illuminate\Http\Request;

class TractionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $tractions = Traction::all();
        return view('tractions.index', compact('tractions', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('tractions.add', compact('module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3'
        ]);

        $tractions = Traction::create([
            'description'=>$request->description,
        ]);

        $audits = new Audits;

        $audits->save_audits('Add new Tractions:'.$tractions->id." - ".$request->description);
        return redirect('tractions');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $tractions = Traction::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('tractions.mod', compact('tractions', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3'
        ]);

        Traction::where('id', $request->id)->update([
            'description'=>$request->description
        ]);

        $audits = new Audits;

        $audits->save_audits('Modify Tractions:'.$request->id." - ".$request->description);
        return redirect('tractions');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $tractions = Traction::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('tractions.del', compact('tractions', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id'=>'required:numeric|exists:tractions,id',
            'description' => 'required|min:3'
        ]);

        Traction::find($request->id)->delete();
        $audits = new Audits;

        $audits->save_audits('Delete Tractions:'.$request->id." - ".$request->description);
        return redirect('tractions');
    }
}
