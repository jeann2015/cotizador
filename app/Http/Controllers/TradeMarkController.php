<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Modules;
use App\TradeMark;
use Illuminate\Http\Request;

class TradeMarkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $trademarks = TradeMark::all();
        return view('marks.index', compact('trademarks', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('marks.add', compact('module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:2',
            'facebook' => 'required|min:3',
            'twitter' => 'required|min:3',
            'instagram' => 'required|min:3',
        ]);

        $trademarks = TradeMark::create([
            'description'=>$request->description,
            'facebook'=>$request->facebook,
            'twitter'=>$request->twitter,
            'instagram'=>$request->instagram
        ]);

        $audits = new Audits;

        $audits->save_audits('Add new Cabin:'.$trademarks->id." - ".$request->description);
        return redirect('marks');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $trademarks = TradeMark::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('marks.mod', compact('trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:2',
            'facebook' => 'required|min:3',
            'twitter' => 'required|min:3',
            'instagram' => 'required|min:3'
        ]);

        TradeMark::where('id', $request->id)->update([
            'description'=>$request->description,
            'facebook'=>$request->facebook,
            'twitter'=>$request->twitter,
            'instagram'=>$request->instagram
        ]);

        $audits = new Audits;

        $audits->save_audits('Modify Marks:'.$request->id." - ".$request->description);
        return redirect('marks');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $trademarks = TradeMark::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('marks.del', compact('trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id'=>'required:numeric|exists:trade_marks,id',
            'description' => 'required|min:2',
            'facebook' => 'required|min:3',
            'twitter' => 'required|min:3',
            'instagram' => 'required|min:3'
        ]);

        TradeMark::find($request->id)->delete();
        $audits = new Audits;

        $audits->save_audits('Delete Marks:'.$request->id." - ".$request->description);
        return redirect('marks');
    }
}
