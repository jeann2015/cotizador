<?php

namespace App\Http\Controllers;

use App\Color;
use App\Office;
use App\Pattern;
use App\PatternDetails;
use App\PatternImagesColor;
use App\Quotes;
use App\QuotesPattern;
use App\SellerOffice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Modules;
use Storage;

class QuotesCustomController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required|min:3',
            'lname'=>'required|min:3',
            'phone'=>'required|min:3',
            'id_card'=>'required|min:3',
            'email'=>'required|email',
            'offices_id'=>'required:numeric|exists:offices,id',
        ]);

        if (!isset($request->contacted)) {
            $var_contacted=0;
        } else {
            $var_contacted = $request->contacted;
        }

        $link = hash('md5', uniqid('142857', true));

        $quotes = Quotes::create([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'id_card' => $request->id_card,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'offices_id'=>$request->offices_id,
            'lon'=>$request->lon,
            'contacted'=>$var_contacted,
            'link'=>$link
        ]);

        $patterns = Pattern::all();
        $quotespattern = new QuotesPattern;
        $quote = new Quotes;

        $seller_id = $quotespattern->get_sellers_quotes($request->offices_id);

        foreach ($patterns as $pattern) {
            $car='car'.$pattern->id;
            $models = $request->$car;
            if ($models<>"") {
                QuotesPattern::insert([
                    'quotes_id' => $quotes->id,
                    'patterns_id' => $pattern->id,
                    'sellers_id' => $seller_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }
        }
        $name_custom = $request->fname." ".$request->lname;
        $email = $request->email;
        $quote->send_mail($request->fname, $name_custom, $email, $seller_id, $link);

        flash('La Cotización ha sido Enviada a tu Correo, gracias!');

        return redirect('quotes/askfor');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function askFor(Request $request)
    {
        $module = new Modules;
        $office = new Office;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $offices = $office->get_offices(1)->pluck('name', 'id');
        $patterndetail = new PatternDetails;
        $patterndetails = $patterndetail->get_patters_only(1);
        $con=1;
        foreach ($patterndetails as $patterndetail) {
            if ($patterndetail->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[$con] = $link . '.jpg';
                \Image::make($patterndetail->images1, 100, 100)->save($pathToFile[$con]);
            }
            $con=$con+1;
        }

        return view('quotes.askfor', compact('pathToFile', 'patterndetails', 'offices', 'offices', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function askFormg(Request $request)
    {
        $module = new Modules;
        $office = new Office;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $offices = $office->get_offices(2)->pluck('name', 'id');
        $patterndetail = new PatternDetails;
        $patterndetails = $patterndetail->get_patters_only(2);
        $con=1;
        foreach ($patterndetails as $patterndetail) {
            if ($patterndetail->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[$con] = $link . '.jpg';
                \Image::make($patterndetail->images1, 100, 100)->save($pathToFile[$con]);
            }
            $con=$con+1;
        }

        return view('quotes.askformg', compact('pathToFile', 'patterndetails', 'offices', 'offices', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function send(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('quotes.askfor', compact('user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details(Request $request)
    {
        if ($request->id<>"") {

            $quotes = Quotes::join('quotes_patterns', 'quotes_patterns.quotes_id', 'quotes.id')
                ->join('patterns', 'quotes_patterns.patterns_id', 'patterns.id')
                ->join('trade_marks', 'patterns.trade_marks_id', 'trade_marks.id')
                ->select('quotes_patterns.sellers_id', 'quotes.*',
                    'patterns.images1', 'patterns.id as patterns_id', 'patterns.description',
                    'trade_marks.facebook','trade_marks.twitter','trade_marks.instagram')
                ->where('link', '=', $request->id)->get();


            if ($quotes->count() > 0) {
                $sellers_id = "";
                $fname_custom = "";

                $name = "";
                $name_seller = "";
                $pattern = new PatternDetails;

                foreach ($quotes as $quote) {
                    $facebook = $quote->facebook;
                    $twitter = $quote->twitter;
                    $instagram = $quote->instagram;
                    $sellers_id = $quote->sellers_id;
                    $fname_custom = $quote->fname . " " . $quote->lname;
                    $name = $quote->fname;
                    $patterns_id = $quote->patterns_id;
                    $colors_patterns_id[]=$quote->patterns_id;

                    $subpatterns = $pattern->get_patters_sub_patterns_images($patterns_id);
                    $con=0;
                    if ($subpatterns->count()>0) {
                        foreach ($subpatterns as $subpattern) {
                            if ($subpattern->images1 <> "") {
                                if ($con==0) {
                                    $directory = public_path() . "/assets/images/cars/" . $patterns_id ."/";
                                    \File::deleteDirectory($directory);
                                    \File::makeDirectory($directory, 0755, true);
                                }

                                $link = str_pad($con, 3, '0', STR_PAD_LEFT);
                                $pathToFileP[$patterns_id] = $directory . $link . '.jpg';
                                $pathToFilePF[$patterns_id] = "/assets/images/cars/" . $patterns_id ."/" . $link . '.jpg';

                                \Image::make($subpattern->images1, 100, 100)->save($pathToFileP[$patterns_id]);
                            }
                            $con = $con + 1;
                        }
                    }

                    $pattern->images_create($patterns_id);
                    $patterns_id_all[]=$patterns_id;

                }


                $subpatters = PatternDetails::
                whereIN('pattern_details.patterns_id', $patterns_id_all)
                    ->select(
                        'pattern_details.id','pattern_details.patterns_id',
                        'pattern_details.features','pattern_details.prize'
                    )
                    ->get();


                $subpatternsdocument = PatternImagesColor::whereIN('patterns_id', $patterns_id_all)
                    ->select('id','sub_patterns_id','patterns_id')->get();


                $sellersOffices = SellerOffice::join('offices', 'seller_offices.offices_id', 'offices.id')
                    ->join('sellers', 'seller_offices.sellers_id', 'sellers.id')
                    ->where('seller_offices.offices_id',$quote->offices_id)
                    ->where('seller_offices.sellers_id', $sellers_id)
                    ->select('offices.name', 'offices.address', 'offices.schedule','offices.phone as offices_phone','offices.lon','offices.lat',
                        'sellers.fname', 'sellers.lname', 'sellers.phone',
                        'sellers.email', 'sellers.images')
                    ->get();

                foreach ($sellersOffices as $sellersOffice) {

                    $name_offices = $sellersOffice->name;
                    $addres_offices = $sellersOffice->address;
                    $schedule = $sellersOffice->schedule;
                    $name_seller = $sellersOffice->fname . " " . $sellersOffice->lname;
                    $phone = $sellersOffice->phone;
                    $email = $sellersOffice->email;
                    $offices_phone = $sellersOffice->offices_phone;
                    $lat = $sellersOffice->lat;
                    $lon = $sellersOffice->lon;

                    if ($sellersOffice->images <> "") {
                        $link = hash('md5', uniqid('142857', true));
                        $pathToFileSeleer[1] = $link . '.jpg';
                        \Image::make($sellersOffice->images, 100, 100)->save($pathToFileSeleer[1]);
                    } else {
                        $pathToFileSeleer[1] = "";
                    }
                }

                $data = [
                    'fname_custom' => $fname_custom,
                    'customer' => $name,
                    'name_seller' => $name_seller,
                    'sucursal' => $name_offices,
                    'address' => $addres_offices,
                    'phone' => $phone,
                    'email' => $email,
                    'schedule' => $schedule,
                    'offices_phone' =>$offices_phone,
                    'lat'=>$lat,
                    'lon'=>$lon,
                    'facebook'=>$facebook,
                    'twitter'=>$twitter,
                    'instagram'=>$instagram
                ];


                return view('quotes.details', compact('subpatternsdocument','pathToFilePF','color_patterns','pathToFileSeleer', 'subpatters', 'quotes', 'data'));
            }
        }else{

            return view('quotes.error');
        }
    }
}
