<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Modules;
use App\Office;
use App\OfficeTrader;
use App\TradeMark;
use Illuminate\Http\Request;

class OfficeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $offices = Office::all();
        return view('offices.index', compact('offices', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $trademarks = TradeMark::select('description', 'id')->pluck('description', 'id');
        return view('offices.add', compact('trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'address'=>'required|min:3',
            'schedule'=>'required|min:3',
            'phone'=>'required|min:3',
            'lat'=>'required|min:3',
            'lon'=>'required|min:3',
            'trade_marks_id'=>'required:numeric'
        ]);

        $office = Office::create([
            'name' => $request->name,
            'address'=>$request->address,
            'schedule'=>$request->schedule,
            'phone'=>$request->phone,
            'lat'=>$request->lat,
            'lon'=>$request->lon,
        ]);

        foreach ($request->trade_marks_id as $trade_marks) {
            OfficeTrader::create([
                'offices_id' => $office->id,
                'trade_marks_id' => $trade_marks
            ]);
        }

        $audits = new Audits;
        $audits->save_audits('Add new Office:'.$office->id." - ".$request->description);
        return redirect('offices');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $offices = Office::find($request->id);
        $officetraders = OfficeTrader::where('offices_id', $request->id)->pluck('trade_marks_id');

        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $trademarks = TradeMark::select('description', 'id')->pluck('description', 'id');

        return view('offices.mod', compact('officetraders', 'trademarks', 'offices', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'address'=>'required|min:3',
            'schedule'=>'required|min:3',
            'phone'=>'required|min:3',
            'lat'=>'required|min:3',
            'lon'=>'required|min:3',
            'id'=>'required|numeric|exists:offices,id'
        ]);

        Office::where('id', $request->id)->update([
            'name' => $request->name,
            'address'=>$request->address,
            'schedule'=>$request->schedule,
            'phone'=>$request->phone,
            'lat'=>$request->lat,
            'lon'=>$request->lon,
        ]);

        OfficeTrader::where('offices_id', $request->id)->delete();

        foreach ($request->trade_marks_id as $trade_marks) {
            OfficeTrader::create([
                'trade_marks_id' => $trade_marks,
                'offices_id' => $request->id
            ]);
        }

        $audits = new Audits;

        $audits->save_audits('Modify Office:'.$request->id." - ".$request->description);
        return redirect('offices');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $offices = Office::find($request->id);
        $officetraders = OfficeTrader::where('offices_id', $request->id)->pluck('trade_marks_id');

        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $trademarks = TradeMark::select('description', 'id')->pluck('description', 'id');

        return view('offices.del', compact('officetraders', 'trademarks', 'offices', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'address'=>'required|min:3',
            'schedule'=>'required|min:3',
            'lat'=>'required|min:3',
            'lon'=>'required|min:3',
            'id'=>'required|numeric|exists:offices,id'
        ]);

        OfficeTrader::where('offices_id', $request->id)->delete();
        Office::find($request->id)->delete();
        $audits = new Audits;
        $audits->save_audits('Delete Office:'.$request->id." - ".$request->description);
        return redirect('offices');
    }
}
