<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;

use App\Modules;

class AuditsController extends Controller
{
    /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index(Request $request)
    {
        $audits_news = new Audits;
        $module = new Modules;

        $iduser = \Auth::id();
        $url = $request->path();


        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $user_access = $module->accesos($iduser, $url);



        $audits_news->save_audits('Views Auditoria');

        $audits = \DB::table('audits')
    ->join('users', 'audits.id_user', '=', 'users.id')
    ->select('audits.*', 'users.name')
    ->orderBy('id', 'desc')
    ->paginate(15);
        return view('audits.index', compact('audits', 'user_access', 'module_menus', 'module_principals'));
    }
}
