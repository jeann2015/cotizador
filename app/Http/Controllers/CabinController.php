<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Modules;
use Illuminate\Http\Request;

use App\Cabin;

class CabinController extends Controller
{
    /**
     * CabinController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $cabins = Cabin::all();
        return view('cabins.index', compact('cabins', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('cabins.add', compact('module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3'
        ]);

        $cabins = Cabin::create([
            'description'=>$request->description,
        ]);

        $audits = new Audits;

        $audits->save_audits('Add new Cabin:'.$cabins->id." - ".$request->description);
        return redirect('cabins');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $cabins = Cabin::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('cabins.mod', compact('cabins', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3'
        ]);

        Cabin::where('id', $request->id)->update([
            'description'=>$request->description
        ]);

        $audits = new Audits;

        $audits->save_audits('Modify Cabin:'.$request->id." - ".$request->description);
        return redirect('cabins');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $cabins = Cabin::find($request->id);
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('cabins.del', compact('cabins', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id'=>'required:numeric|exists:cabins,id',
            'description' => 'required|min:3'
        ]);

        Cabin::find($request->id)->delete();
        $audits = new Audits;

        $audits->save_audits('Delete Cabin:'.$request->id." - ".$request->description);
        return redirect('cabins');
    }
}
