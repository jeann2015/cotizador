<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Color;
use App\Modules;
use App\Pattern;
use App\PatternDetails;
use App\PatternImagesColor;
use App\SubPatternDetails;
use App\SubPatternDetailsFiles;
use App\TradeMark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PatternController extends Controller
{
    /**
     * PatternController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $pattern = new Pattern;
        $patterns = $pattern->get_patterns();
        return view('patterns.index', compact('patterns', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());
        return view('patterns.add', compact('trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addsub(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $pattern = new Pattern;
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $patterns = $pattern->get_patterns($request->id);
        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());
        $color = Color::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Color');
        $colors = array_sort_recursive($color->toArray());

        return view('patterns.subadd', compact('colors', 'patterns', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'images1' => 'required:image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'trade_marks_id' => 'required:numeric|exists:trade_marks,id'
        ]);

        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images1 = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);
        } else {
            $images1 ="";
        }

        $patterns = Pattern::create([
            'description'=>$request->description,
            'trade_marks_id'=>$request->trade_marks_id,
            'images1'=>$images1
        ]);

        Storage::delete($pathToFile);

        $audits = new Audits;

        $audits->save_audits('Add new Patterns:'.$patterns->id." - ".$request->description);
        return redirect('patterns');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatesub(Request $request)
    {
        $this->validate($request, [
            'features' => 'required|min:3',
            'patterns_id' => 'required:numeric|exists:patterns,id',
            'colors_id' => 'required:numeric|exists:colors,id',
            'images1.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'prize'=>'required:numeric',
            'files'=>'mimes:pdf|max:200000',
        ]);

        $PatternDetails = PatternDetails::find($request->id);
        $PatternDetails->features = $request->features;
        $PatternDetails->prize = $request->prize;
        $PatternDetails->status = $request->status;
        $PatternDetails->save();


        if ($request->file('images1')) {
            PatternImagesColor::where('sub_patterns_id', $request->id)->where('patterns_id', $PatternDetails->patterns_id)->delete();
            $con_files = 1;
            foreach ($request->images1 as $images) {
                if ($con_files <= 5) {
                    $link = hash('md5', uniqid('142857', true));
                    $pathToFile = $link . '.jpg';
                    $images1 = \Image::make($images->getRealPath())
                        ->save($pathToFile);

                    PatternImagesColor::create([
                        'patterns_id' => $request->patterns_id,
                        'sub_patterns_id' => $request->id,
                        'colors_id' => $request->colors_id,
                        'images' => $images1,
                    ]);

                    Storage::delete($pathToFile);
                    $con_files = $con_files + 1;
                }
            }
        }

        $f = $request->file('files');
        if ($f<>"") {
            SubPatternDetailsFiles::where('sub_patterns_id', $request->id)->where('patterns_id', $PatternDetails->patterns_id)->delete();
            SubPatternDetailsFiles::create([
                'sub_patterns_id'=>$request->id,
                'patterns_id'=>$PatternDetails->patterns_id,
                'files' => base64_encode(file_get_contents($f->getRealPath())),
                'type_file' => $f->getClientMimeType(),
                'size' => $f->getClientSize()
            ]);
        }

        $audits = new Audits;
        $audits->save_audits('Update Sub-Patterns:'.$request->id." - ".$request->features);

        return redirect('patterns/edit/'.$request->patterns_id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function newsub(Request $request)
    {
        $this->validate($request, [
            'features' => 'required|min:3',
            'patterns_id' => 'required:numeric|exists:patterns,id',
            'colors_id' => 'required:numeric|exists:colors,id',
            'images1.*' => 'required:image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'prize'=>'required:numeric',
            'files'=>'required|mimes:pdf|max:200000',
        ]);

        DB::beginTransaction();

        try {

            $patterndetails = PatternDetails::create([
                'features' => $request->features,
                'patterns_id' => $request->patterns_id,
                'prize' => $request->prize,
                'status'=>1
            ]);

            if ($request->file('images1')) {
                $con_files = 1;
                foreach ($request->images1 as $images) {
                    if ($con_files <= 5) {
                        $link = hash('md5', uniqid('142857', true));
                        $pathToFile = $link . '.jpg';
                        $images1 = \Image::make($images->getRealPath())
                            ->save($pathToFile);

                        PatternImagesColor::create([
                            'patterns_id' => $request->patterns_id,
                            'sub_patterns_id' => $patterndetails->id,
                            'colors_id' => $request->colors_id,
                            'images' => $images1,
                        ]);

                        Storage::delete($pathToFile);
                        $con_files = $con_files + 1;
                    }
                }
            }

            $f = $request->file('files');

            if ($f <> "") {
                SubPatternDetailsFiles::create([
                    'patterns_id' => $request->patterns_id,
                    'sub_patterns_id' => $patterndetails->id,
                    'files' => base64_encode(file_get_contents($f->getRealPath())),
                    'type_file' => $f->getClientMimeType(),
                    'size' => $f->getClientSize()
                ]);
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors(['error' => $e->getMessage()]);

        }

        return redirect('patterns/edit/'.$request->patterns_id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $m = new Modules();
        $iduser = \Auth::id();
        $patterndetail = new PatternDetails;

        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $pattern = new Pattern;
        $patterndetails = new PatternDetails;

        $patterns = $pattern->get_patterns($request->id);

        foreach ($patterns as $pattern) {
            if ($pattern->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[1] = $link . '.jpg';
                \Image::make($pattern->images1, 100, 100)->save($pathToFile[1]);
            } else {
                $pathToFile[1] = "";
            }
        }
        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());

        $patters_details = $patterndetails->get_patters_details($request->id);
        if(count($patters_details)>0) {
            $patters_details_images = $patterndetail->get_patters_details_id_images($patterndetails->where('patterns_id', $request->id)->pluck('id')[0], $request->id);
        }else{
            $patters_details_images="";
        }

        return view('patterns.mod', compact('patters_details_images','pathToFile', 'patters_details', 'trademarks', 'patterns', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deletesub(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $pattern = new Pattern;
        $patterndetail = new PatternDetails;
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $patterns = $pattern->get_patterns($request->idmodel);
        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());
        $color = Color::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Color');
        $colors = array_sort_recursive($color->toArray());
        $patters_details = $patterndetail->get_patters_details_id($request->id);
        $patters_details_images = $patterndetail->get_patters_details_id_images($request->id);
        $con=1;
        foreach ($patters_details_images as $patters_detail) {
            if ($patters_detail->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[$con] = $link . '.jpg';
                \Image::make($patters_detail->images1, 100, 100)->save($pathToFile[$con]);
            }
            $con=$con+1;
        }
        return view('patterns.subdel', compact('patters_details_images', 'pathToFile', 'patters_details', 'colors', 'patterns', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editsub(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $pattern = new Pattern;
        $patterndetail = new PatternDetails;
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $patterns = $pattern->get_patterns($request->idmodel);
        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());
        $color = Color::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Color');
        $colors = array_sort_recursive($color->toArray());
        $patters_details = $patterndetail->get_patters_details_id($request->id);
        $patters_details_images = $patterndetail->get_patters_details_id_images($request->id,$request->idmodel);


        $con=1;
        foreach ($patters_details_images as $patters_detail) {
            if ($patters_detail->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[$con] = $link . '.jpg';
                \Image::make($patters_detail->images1, 100, 100)->save($pathToFile[$con]);
            }
            $con=$con+1;
        }

        return view('patterns.submod', compact('patters_details_images', 'pathToFile', 'patters_details', 'colors', 'patterns', 'trademarks', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'trade_marks_id' => 'required:numeric|exists:trade_marks,id',
            'images1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id' => 'required:numeric|exists:patterns,id',
        ]);

        if ($request->file('images1')) {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            $images1 = \Image::make($request->file('images1')->getRealPath())
                ->save($pathToFile);
        } else {
            $images1="";
        }


        $patterns = Pattern::find($request->id);
        $patterns->description=$request->description;
        $patterns->trade_marks_id=$request->trade_marks_id;
        if ($images1 <> "") {
            $patterns->images1=$images1;
        }
        $patterns->save();
        
        if ($images1<>"") {
            Storage::delete($pathToFile);
        }



        $audits = new Audits;
        $audits->save_audits('Modify Patterns:'.$request->id." - ".$request->description);
        return redirect('patterns');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $pattern = new Pattern;
        $patterns = $pattern->get_patterns($request->id);

        $trademark = TradeMark::select('description', 'id')->pluck('description', 'id')->put('', 'Seleccione Marca');
        $trademarks = array_sort_recursive($trademark->toArray());

        return view('patterns.del', compact('trademarks', 'patterns', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'trade_marks_id' => 'required:numeric|exists:trade_marks,id',
            'id' => 'required:numeric|exists:patterns,id',
        ]);

        SubPatternDetailsFiles::where('patterns_id', $request->id)->delete();
        PatternImagesColor::where('patterns_id', $request->id)->delete();
        PatternDetails::where('patterns_id', $request->id)->delete();
        Pattern::find($request->id)->delete();

        $audits = new Audits;
        $audits->save_audits('Delete Patterns:'.$request->id." - ".$request->description);
        return redirect('patterns');
    }

    public function destroysub(Request $request)
    {
        $this->validate($request, [
            'features' => 'required|min:3',
            'patterns_id' => 'required:numeric|exists:patterns,id',
            'colors_id' => 'required:numeric|exists:colors,id'
        ]);

        SubPatternDetailsFiles::where('patterns_id', $request->id)->delete();
        PatternImagesColor::where('sub_patterns_id', $request->id)->delete();
        PatternDetails::find($request->id)->delete();

        $audits = new Audits;
        $audits->save_audits('Delete Sub-Patterns:'.$request->id." - ".$request->features);
        return redirect('patterns/edit/'.$request->patterns_id);
    }
}
