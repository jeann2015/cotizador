<?php

namespace App\Http\Controllers;

use App\Office;
use App\PatternDetails;
use App\Quotes;
use Illuminate\Http\Request;
use App\Modules;

class CotizacionesController extends Controller
{

    /**
     * CotizacionesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $quote = new Quotes;
        $quotes = $quote->get_quotes_user_seller();
        return view('quote.index', compact('quotes', 'user_access', 'module_menus', 'module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $offices = Office::select('name', 'id')->pluck('name', 'id');

        $quote = new Quotes;
        $quotes=$quote->get_quotes_id($request->id);

        foreach ($quotes as $quote) {
            $patterns_id = $quote->patterns_id;
        }

        $patterndetail = new PatternDetails;
        $patterndetails = $patterndetail->get_patters_details_id($patterns_id);
        $con=1;
        foreach ($patterndetails as $patterndetail) {
            if ($patterndetail->images1 <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[$con] = $link . '.jpg';
                \Image::make($patterndetail->images1, 100, 100)->save($pathToFile[$con]);
            }
            $con=$con+1;
        }

        return view('quote.view', compact('pathToFile', 'quotes', 'offices', 'offices', 'user_access', 'module_menus', 'module_principals'));
    }
}
