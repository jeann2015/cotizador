<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Modules;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);

        $module_menus = $m->get_modules_menu_user($iduser);
        return view('home', compact('module_principals', 'module_menus'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changes()
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('changes.index', compact('module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $iduser = \Auth::id();
        $user = User::find($iduser);
        $user->password = bcrypt($request->password);
        $user->save();
        flash('Clave Modificada!');
        return redirect('changes');
    }
}
