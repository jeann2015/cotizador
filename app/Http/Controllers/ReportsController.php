<?php

namespace App\Http\Controllers;

use App\Office;
use App\Quotes;
use App\Seller;
use Illuminate\Http\Request;
use App\Modules;
use Carbon\Carbon;


class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function reports(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $office = Office::select('name', 'id')->pluck('name', 'id')->put('0', 'Todas las Sucursales');
        $offices = array_sort_recursive($office->toArray());

        $values['desde']="";
        $values['hasta']="";

        return view('reports.especifico',
            compact('values','offices','module_principals', 'module_menus'));
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'offices_id' => 'required:numeric',
            'sellers_id' => 'required:numeric',
            'desde'=>'required:date',
            'hasta'=>'required:date'
        ]);

        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $office = Office::select('name', 'id')->pluck('name', 'id')->put('0', 'Todas las Sucursales');
        $offices = array_sort_recursive($office->toArray());

        $seller = Seller::select( \DB::raw('concat(sellers.fname," ",sellers.lname) as name_sellers'), 'id')->pluck('name_sellers', 'id')->put('0', 'Todos Los Vendedores');
        $sellers = array_sort_recursive($seller->toArray());

        $values['desde']=$request->desde;
        $values['hasta']=$request->hasta;
        $values['offices_id']=$request->offices_id;
        $values['sellers_id']=$request->sellers_id;



        $quote = new Quotes;
        $quotes = $quote->get_quotes_user_seller_report($request->offices_id,$request->sellers_id,$request->desde,$request->hasta);

        return view('reports.especifico', compact('sellers','values','offices','quotes', 'user_access', 'module_menus', 'module_principals'));
    }

}
