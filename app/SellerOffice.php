<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerOffice extends Model
{
    protected $table="seller_offices";

    protected $fillable = [
        'offices_id','sellers_id'
    ];
}
