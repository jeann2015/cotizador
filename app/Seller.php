<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table="sellers";

    protected $fillable = [
        'fname','lname','email','offices_id','images','status','type','phone'
    ];

    /**
     * @return mixed
     */
    public function get_sellers_offices()
    {
        $sellers = self::join('seller_offices', function ($join) {
            $join->on('seller_offices.sellers_id', '=', 'sellers.id');
        })
        ->join('offices', function ($join) {
            $join->on('seller_offices.offices_id', '=', 'offices.id');
        })
        ->select('sellers.*', 'offices.name as description_offices')
        ->get();

        return $sellers;
    }

    /**
     * @param $offices_id
     * @return mixed
     */
    public function get_sellers_offices_status($offices_id)
    {
        $sellers = self::join('seller_offices', function ($join) {
            $join->on('seller_offices.sellers_id', '=', 'sellers.id');
        })
            ->join('offices', function ($join) use ($offices_id) {
                if ($offices_id<>"0") {
                    $join->on('seller_offices.offices_id', '=', 'offices.id')
                        ->where('seller_offices.offices_id', '=', $offices_id);
                } else {
                    $join->on('seller_offices.offices_id', '=', 'offices.id');
                }
            })
            ->where('status', 1)
            ->select('sellers.id', 'sellers.fname', 'sellers.lname')
            ->get();

        return $sellers;
    }
}
