<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatternImagesColor extends Model
{
    protected $table="pattern_images_colors";

    protected $fillable = [
        'patterns_id','colors_id','images','sub_patterns_id'
    ];
}
