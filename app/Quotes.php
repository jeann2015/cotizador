<?php

namespace App;

use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    protected $table="quotes";

    protected $fillable = [
        'fname','lname','email','phone','id_card','offices_id','contacted','link'
    ];

    /**
     * @param $quotes_id
     * @return mixed
     */
    public function get_quotes_id($quotes_id=0)
    {
        if ($quotes_id<>"0") {
            $quotes = self::
            join('quotes_patterns', function ($join) use ($quotes_id) {
                $join->on('quotes_patterns.quotes_id', '=', 'quotes.id')
                    ->where('quotes_patterns.quotes_id', $quotes_id);
            })
                ->join('sellers', function ($join) {
                    $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                })
                ->join('user_sellers', function ($join) {
                    $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id');
                })
                ->join('offices', function ($join) {
                    $join->on('quotes.offices_id', '=', 'offices.id');
                })
                ->join('patterns', function ($join) {
                    $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                })
                ->select('sellers.type', 'sellers.id as sellers_id', 'quotes_patterns.patterns_id', 'user_sellers.users_id',
                    'patterns.description as patterns_description', 'quotes.email as email_custom',
                    'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'quotes.phone', 'quotes.id_card',
                    'sellers.fname', 'sellers.lname', 'offices.name as description_offices', 'quotes.contacted')
                ->orderby('quotes.id', 'asc')
                ->get();
        } else {
            $quotes=array();
        }

        return $quotes;
    }

    /**
     * @return array
     */
    public function get_quotes_user_seller()
    {
        $iduser = \Auth::id();

        $type_users = UserSeller::join('sellers', 'sellers.id', 'user_sellers.sellers_id')
            ->join('seller_offices', 'seller_offices.sellers_id', 'sellers.id')
            ->where('user_sellers.users_id', $iduser)
            ->select('sellers.type', 'sellers.id', 'seller_offices.offices_id as offices_id')
            ->get();

        if ($type_users->count()>0) {
            foreach ($type_users as $type_user) {
                $type = $type_user->type;
                $offices_id = $type_user->offices_id;
            }
            if ($type==0) {
                $seller = self::
                join('quotes_patterns', function ($join) {
                    $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                })
                    ->join('sellers', function ($join) {
                        $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                    })
                    ->join('user_sellers', function ($join) use ($iduser) {
                        $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id')
                            ->where('user_sellers.users_id', '=', $iduser);
                    })
                    ->join('offices', function ($join) {
                        $join->on('quotes.offices_id', '=', 'offices.id');
                    })
                    ->join('patterns', function ($join) {
                        $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                    })
                    ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                    ->orderby('quotes.id', 'desc')
                    ->get();
            } elseif ($type==1) {
                $seller = self::
                join('quotes_patterns', function ($join) {
                    $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                })
                    ->join('sellers', function ($join) {
                        $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                    })
                    ->join('user_sellers', function ($join) {
                        $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id');
                    })
                    ->join('offices', function ($join) {
                        $join->on('quotes.offices_id', '=', 'offices.id');
                    })
                    ->join('patterns', function ($join) {
                        $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                    })
                    ->where('quotes.offices_id', $offices_id)
                    ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                    ->orderby('quotes.id', 'desc')
                    ->get();
            }
        } else {
            $GruposUsers = GruposUsers::where('id_user', $iduser)->get();
            foreach ($GruposUsers as $GruposUser) {
                if ($GruposUser->id_grupos == 1) {
                    $seller = self::
                    join('quotes_patterns', function ($join) {
                        $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                    })
                        ->join('sellers', function ($join) {
                            $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                        })
                        ->join('user_sellers', function ($join) {
                            $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id');
                        })
                        ->join('offices', function ($join) {
                            $join->on('quotes.offices_id', '=', 'offices.id');
                        })
                        ->join('patterns', function ($join) {
                            $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                        })
                        ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                        ->orderby('quotes.id', 'desc')
                        ->get();
                } else {
                    $seller=array();
                }
            }
        }

        return $seller;
    }

    public function send_mail($fname_custom, $name, $email, $sellers_id, $linkdetails)
    {
        $sellers = Seller::find($sellers_id);

        $sellersOffices = SellerOffice::join('offices', 'seller_offices.offices_id', 'offices.id')
            ->join('sellers', 'seller_offices.sellers_id', 'sellers.id')
            ->join('office_traders', 'seller_offices.offices_id', 'office_traders.offices_id')
            ->join('trade_marks', 'office_traders.trade_marks_id', 'trade_marks.id')
            ->where('sellers_id', $sellers->id)
            ->select('offices.name', 'offices.address', 'offices.schedule',
                'offices.phone','offices.lat','offices.lon','trade_marks.facebook',
                'trade_marks.twitter','trade_marks.instagram')
            ->get();

        foreach ($sellersOffices as $sellersOffice) {
            $name_offices=$sellersOffice->name;
            $addres_offices=$sellersOffice->address;
            $schedule=$sellersOffice->schedule;
            $offices_phone = $sellersOffice->phone;
            $lat = $sellersOffice->lat;
            $lon = $sellersOffice->lon;
            $instagram = $sellersOffice->instagram;
            $twitter = $sellersOffice->twitter;
            $facebook = $sellersOffice->facebook;
        }

        if ($sellers->images <> "") {
            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';
            \Image::make($sellers->images, 100, 100)->save($pathToFile);
        } else {
            $pathToFile = "";
        }

        $data = [
            'fname_custom'=>$fname_custom,
            'customer' => 'Hola '.$name,
            'name_seller' => 'Soy: '.$sellers->fname." ".$sellers->lname,
            'sucursal' => $name_offices,
            'address' => $addres_offices,
            'phone' => $sellers->phone,
            'phone_offices' => $offices_phone,
            'email' => $sellers->email,
            'picture_seller'=>$pathToFile,
            'link'=>$linkdetails,
            'schedule'=>$schedule,
            'lat'=>$lat,
            'lon'=>$lon,
            'facebook'=>$facebook,
            'twitter'=>$twitter,
            'instagram'=>$instagram,
        ];

        config(['mailgun.from.name'=>'Cotización Online '.$name]);

        Mailgun::send('emails.mail', $data, function ($message) use ($name, $email) {
            $message->to($email, $name)->subject('Gracias por Cotizar en Isuzu!');
        });
    }

    /**
     * @param int $search_offices_id
     * @param int $sellers_id
     * @param $desde
     * @param $hasta
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get_quotes_user_seller_report($search_offices_id=0,$sellers_id=0,$desde,$hasta)
    {
        $iduser = \Auth::id();

        $type_users = UserSeller::join('sellers', 'sellers.id', 'user_sellers.sellers_id')
            ->join('seller_offices', 'seller_offices.sellers_id', 'sellers.id')
            ->where('user_sellers.users_id', $iduser)
            ->select('sellers.type', 'sellers.id', 'seller_offices.offices_id as offices_id')
            ->get();

        if ($type_users->count()>0) {
            foreach ($type_users as $type_user) {
                $type = $type_user->type;
                $offices_id = $type_user->offices_id;
            }
            if ($type==0) {
                $seller = self::
                join('quotes_patterns', function ($join) {
                    $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                })
                    ->join('sellers', function ($join) use($sellers_id) {
                        if($sellers_id<>"0") {
                            $join->on('quotes_patterns.sellers_id', '=', 'sellers.id')
                            ->where('sellers.id',$sellers_id);
                        }else{
                            $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                        }
                    })
                    ->join('user_sellers', function ($join) use ($iduser) {
                        $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id')
                            ->where('user_sellers.users_id', '=', $iduser);
                    })
                    ->join('offices', function ($join) use($search_offices_id){
                        if($search_offices_id<>"0") {
                            $join->on('quotes.offices_id', '=', 'offices.id')
                            ->where('offices.id',$search_offices_id);
                        }else{
                            $join->on('quotes.offices_id', '=', 'offices.id');
                        }
                    })
                    ->join('patterns', function ($join) {
                        $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                    })
                    ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                    ->orderby('quotes.id', 'desc')
                    ->whereBetween('quotes.created_at',[$desde." 00:00:00",$hasta." 23:59:59"] )
                    ->get();
            } elseif ($type==1) {
                $seller = self::
                join('quotes_patterns', function ($join) {
                    $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                })
                    ->join('sellers', function ($join) use($sellers_id) {
                        if($sellers_id<>"0") {
                            $join->on('quotes_patterns.sellers_id', '=', 'sellers.id')
                                ->where('sellers.id',$sellers_id);
                        }else{
                            $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                        }
                    })
                    ->join('user_sellers', function ($join) {
                        $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id');
                    })
                    ->join('offices', function ($join) use($search_offices_id){
                        if($search_offices_id<>"0") {
                            $join->on('quotes.offices_id', '=', 'offices.id')
                                ->where('offices.id',$search_offices_id);
                        }else{
                            $join->on('quotes.offices_id', '=', 'offices.id');
                        }
                    })
                    ->join('patterns', function ($join) {
                        $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                    })
                    ->where('quotes.offices_id', $offices_id)
                    ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                    ->orderby('quotes.id', 'desc')
                    ->whereBetween('quotes.created_at',[$desde." 00:00:00",$hasta." 23:59:59"] )
                    ->get();
            }
        } else {
            $GruposUsers = GruposUsers::where('id_user', $iduser)->get();

            foreach ($GruposUsers as $GruposUser) {
                if ($GruposUser->id_grupos == 1) {
                    $seller = self::
                    join('quotes_patterns', function ($join) {
                        $join->on('quotes_patterns.quotes_id', '=', 'quotes.id');
                    })
                        ->join('sellers', function ($join) use($sellers_id) {
                            if($sellers_id<>"0") {
                                $join->on('quotes_patterns.sellers_id', '=', 'sellers.id')
                                    ->where('sellers.id',$sellers_id);
                            }else{
                                $join->on('quotes_patterns.sellers_id', '=', 'sellers.id');
                            }
                        })
                        ->join('user_sellers', function ($join) {
                            $join->on('quotes_patterns.sellers_id', '=', 'user_sellers.sellers_id');
                        })
                        ->join('offices', function ($join) use($search_offices_id){
                            if($search_offices_id<>"0") {
                                $join->on('quotes.offices_id', '=', 'offices.id')
                                    ->where('offices.id',$search_offices_id);
                            }else{
                                $join->on('quotes.offices_id', '=', 'offices.id');
                            }
                        })
                        ->join('patterns', function ($join) {
                            $join->on('quotes_patterns.patterns_id', '=', 'patterns.id');
                        })
                        ->select('quotes.id', 'sellers.type', 'sellers.id as sellers_id', 'user_sellers.users_id', 'patterns.description as patterns_description', 'quotes.*', 'quotes.email as email_custom', 'quotes.fname as fname_custom', 'quotes.lname as lname_custom', 'sellers.fname', 'sellers.lname', 'offices.name as description_offices')
                        ->orderby('quotes.id', 'desc')
                        ->whereBetween('quotes.created_at',[$desde." 00:00:00",$hasta." 23:59:59"] )
                        ->get();
                } else {
                    $seller=array();
                }
            }
        }

        return $seller;
    }
}
