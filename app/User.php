<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function news_user_sellers($name, $email, $grupos, $sellers_id)
    {
        $users = new User;
        $audits = new Audits;

        $modules = Modules::all();


        $users->name = $name;
        $users->email = $email;
        $users->password = bcrypt('123456');
        $users->status = '1';
        $users->save();

        $GruposUsers = new GruposUsers;
        $GruposUsers->id_user = $users->id;
        $GruposUsers->id_grupos = $grupos;
        $GruposUsers->save();

        foreach ($modules as $module) {
            $access = new Access;
            $gruposmodu = new GruposModules;

            $gruposmodule = $gruposmodu->grupos_modulos($grupos, $module->id);

            $gruposmodules = Collection::make($gruposmodule);
            foreach ($gruposmodules as $gruposmodule) {
                $view='view'.$module->id;
                $view_value = $gruposmodule->views;
                if ($view_value=="") {
                    $view_value="0";
                }

                $save='save'.$module->id;
                $save_value = $gruposmodule->inserts;
                if ($save_value=="") {
                    $save_value="0";
                }

                $modify='modify'.$module->id;
                $modify_value = $gruposmodule->modifys;
                if ($modify_value=="") {
                    $modify_value="0";
                }

                $delete='delete'.$module->id;
                $delete_value = $gruposmodule->deletes;
                if ($delete_value=="") {
                    $delete_value="0";
                }

                if ($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"") {
                    $access->id_user=$users->id;
                    $access->id_module=$module->id;
                    $access->views=$view_value;
                    $access->inserts=$save_value;
                    $access->modifys=$modify_value;
                    $access->deletes=$delete_value;
                    $access->status=0;
                    $access->save();
                }
            }
        }

//        $audits->save_audits('Add new User:'.$users->id." - ".$name);

        if ($grupos==2) {
            $UserSellers= UserSeller::create([
                'sellers_id'=>$sellers_id,
                'users_id'=>$users->id
            ]);

//            $audits->save_audits('Add new User Sellers:'.$sellers_id." - ".$name);
        }
        return  $users->id;
    }
}
