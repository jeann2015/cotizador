<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSeller extends Model
{
    protected $table="user_sellers";

    protected $fillable = [
        'sellers_id','users_id'
    ];
}
