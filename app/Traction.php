<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traction extends Model
{
    protected $table="tractions";

    protected $fillable = [
        'description'
    ];
}
