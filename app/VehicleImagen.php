<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleImagen extends Model
{
    protected $table="vehicle_imagens";

    protected $fillable = [
        'vehicles_id',
        'images'
    ];
}
