<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuotesPattern extends Model
{
    protected $table="quotes_patterns";

    protected $fillable = [
        'quotes_id','patterns_id','sellers_id'
    ];

    /**
     * @param $offices_id
     * @return mixed
     */
    public function get_sellers_quotes($offices_id)
    {
        $seller = new Seller;
        $now = Carbon::now()->toDateString();
        $sellers = $seller->get_sellers_offices_status($offices_id)->pluck('id');

        $quotes_date = self::select('quotes_patterns.*')
            ->where('quotes_patterns.created_at', '>=', $now." 00:00:00")
            ->get();

        if ($quotes_date->count()>0) {
            foreach ($sellers->toArray() as $seller) {
                $quotes = self::select('quotes_patterns.*')
                    ->where('quotes_patterns.sellers_id', $seller)
                    ->where('quotes_patterns.created_at', '>=', $now." 00:00:00")
                    ->get();

                $sellers_quantity[$seller]  = $quotes->count();
            }

            asort($sellers_quantity);
            $sellers_quantity_r = array_keys($sellers_quantity);
            return $sellers_quantity_r[0];
        } else {
            foreach ($sellers->random(1) as $seller) {
                $sell=$seller;
            }
            return $sell;
        }
    }
}
