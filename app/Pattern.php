<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pattern extends Model
{
    protected $table="patterns";


    protected $fillable = [
        'description','trade_marks_id','images1'
    ];



    public function get_patterns($id=0)
    {
        $patters = Pattern::
            join('trade_marks', function ($join) use ($id) {
                if ($id<>0) {
                    $join->on('patterns.trade_marks_id', '=', 'trade_marks.id')
                    ->where('patterns.id', '=', $id);
                } else {
                    $join->on('patterns.trade_marks_id', '=', 'trade_marks.id');
                }
            })
            ->select('patterns.*', 'trade_marks.description as description_trade_marks',
                'patterns.trade_marks_id', 'patterns.images1')
            ->get();

        return $patters;
    }
}
