<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatternDetails extends Model
{
    protected $table="pattern_details";

    protected $fillable = [
        'patterns_id','features','prize','status'
    ];

    /**
     * @param int $id
     * @return mixed
     */
    public function get_patters_details($id=0)
    {
        $patters = self::
            join('patterns', function ($join) use ($id) {
                if ($id<>0) {
                    $join->on('pattern_details.patterns_id', '=', 'patterns.id')
                        ->where('pattern_details.patterns_id', '=', $id);
                } else {
                    $join->on('pattern_details.patterns_id', '=', 'patterns.id');
                }
            })
            ->join('trade_marks', function ($join) {
                $join->on('patterns.trade_marks_id', '=', 'trade_marks.id');
            })
            ->select('patterns.id as pattern_model_id',
                'patterns.trade_marks_id', 'pattern_details.id as id_details',
                'pattern_details.status','pattern_details.prize','pattern_details.features',
                'trade_marks.description as description_trade_marks')
            ->get();

        return $patters;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get_patters_details_id_images($id=0,$idmodel=0)
    {
        $patters = self::join('pattern_images_colors', function ($join) use($id,$idmodel) {
            $join->on('pattern_details.id', '=', 'pattern_images_colors.sub_patterns_id')
                ->where('pattern_details.patterns_id', '=', $idmodel)
                ->where('pattern_images_colors.sub_patterns_id','=',$id);
        })
        ->join('colors', function ($join) {
            $join->on('pattern_images_colors.colors_id', '=', 'colors.id');
        })

            ->select(
                'pattern_details.patterns_id as pattern_model_id',
                'pattern_details.id as id_details',
                'pattern_images_colors.colors_id',
                'pattern_details.prize',
                'pattern_details.features',
                'colors.description as description_color',
                'pattern_images_colors.images as images1'
                )
            ->get();

        return $patters;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get_patters_details_id($id=0)
    {
        $patters = self::
            join('patterns', function ($join) use ($id) {
                if ($id<>0) {
                    $join->on('pattern_details.patterns_id', '=', 'patterns.id')
                        ->where('pattern_details.id', '=', $id);
                } else {
                    $join->on('pattern_details.patterns_id', '=', 'patterns.id');
                }
           })

            ->join('trade_marks', function ($join) {
                $join->on('patterns.trade_marks_id', '=', 'trade_marks.id');
            })
            ->select(
                'patterns.id as pattern_model_id',
                'patterns.trade_marks_id',
                'pattern_details.id as id_details',
                'pattern_details.prize',
                'pattern_details.status',
                'pattern_details.features',
                'trade_marks.description as description_trade_marks',
                'patterns.description as description_patterns',
                'patterns.images1'
            )
            ->get();

        return $patters;
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Collection
     */
    public function get_patters_only($id=0)
    {
        $patters = Pattern::
            join('trade_marks', function ($join) use($id){
                if($id<>"0") {
                    $join->on('patterns.trade_marks_id', '=', 'trade_marks.id')
                    ->where('trade_marks.id',$id);
                }else{
                    $join->on('patterns.trade_marks_id', '=', 'trade_marks.id');
                }
            })
            ->select('patterns.id as pattern_model_id', 'patterns.images1',
                'patterns.description as description_patterns')
            ->get();

        return $patters;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get_patters_details_id_images_sub($id=0)
    {
        $patters = self::
        join('patterns', function ($join) use ($id) {
            if ($id<>0) {
                $join->on('pattern_details.patterns_id', '=', 'patterns.id')
                    ->where('pattern_details.patterns_id', '=', $id);
            } else {
                $join->on('pattern_details.patterns_id', '=', 'patterns.id');
            }
        })
            ->join('colors', function ($join) {
                $join->on('pattern_details.colors_id', '=', 'colors.id');
            })
            ->join('trade_marks', function ($join) {
                $join->on('patterns.trade_marks_id', '=', 'trade_marks.id');
            })
            ->join('sub_pattern_details_images', function ($join) {
                $join->on('pattern_details.id', '=', 'sub_pattern_details_images.sub_patterns_id');
            })
            ->select('patterns.id as pattern_model_id',
                'patterns.trade_marks_id', 'pattern_details.id as id_details',
                'pattern_details.colors_id', 'pattern_details.prize', 'pattern_details.status',
                'pattern_details.features', 'colors.description as description_color',
                'trade_marks.description as description_trade_marks',
                'patterns.description as description_patterns',
                'colors.color', 'sub_pattern_details_images.id as sub_pattern',
                'sub_pattern_details_images.images1'
            )
            ->get();

        return $patters;
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Collection
     */
    public function get_patters_sub_patterns_images($id=0)
    {
        $patters = Pattern::
            where('patterns.id', $id)
            ->select(
                'patterns.images1'
            )
            ->get();


        return $patters;
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Collection
     */
    public function get_patters_sub_patterns_images_limit($id=0)
    {
        $patters = Pattern::
        join('sub_pattern_details_images', function ($join) {
            $join->on('patterns.id', '=', 'sub_pattern_details_images.patterns_id');
        })
            ->join('pattern_details', function ($join) {
                $join->on('patterns.id', '=', 'pattern_details.patterns_id');
            })
            ->where('patterns.id', $id)
            ->select(
                'patterns.id as pattern_model_id',
                'sub_pattern_details_images.images1',
                'pattern_details.colors_id',
                'pattern_details.status',
                'pattern_details.id as sub_patterns_id'
            )
            ->orderby('sub_pattern_details_images.id')
            ->limit(1)
            ->get();

        return $patters;
    }

    public function images_create($pattern_model_id)
    {
        $patters = self::
        where('pattern_details.patterns_id', $pattern_model_id)
            ->select(
                'pattern_details.id'
            )
            ->get();

        foreach ($patters as $patter) {

            $subpatters = PatternImagesColor::where('patterns_id', $pattern_model_id)
                ->where('sub_patterns_id', $patter->id)
                ->select('pattern_images_colors.images as images1')
                ->limit(5)
                ->get();
            $con=0;
            if ($subpatters->count() > 0) {

                $directorysub = public_path() ."/assets/images/cars/" . $pattern_model_id . "/sub/".$patter->id."/";
                \File::deleteDirectory($directorysub);
                $s = \File::makeDirectory($directorysub, 0755, true);

                foreach ($subpatters as $subpatter) {
                    $linksub = str_pad($con, 3, '0', STR_PAD_LEFT);
                    $pathToSub[$pattern_model_id][$patter->id] = $directorysub . $linksub . '.jpg';
                    $pathToSubF[$pattern_model_id][$patter->id] = "/assets/images/cars/" . $pattern_model_id . "/sub/".$patter->id."/".$linksub . '.jpg';
                    \Image::make($subpatter->images1, 100, 100)->save($pathToSub[$pattern_model_id][$patter->id]);
                    $con=$con+1;
                }

            }
        }
        if(isset($pathToSubF)) {
            return $pathToSubF;
        }else{
            return array();
        }
    }
}
