<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeTrader extends Model
{
    protected $table="office_traders";

    protected $fillable = [
        'offices_id','trade_marks_id'
    ];
}
