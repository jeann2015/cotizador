<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPatternDetailsFiles extends Model
{
    protected $table="sub_pattern_details_files";

    protected $fillable = [
        'patterns_id','sub_patterns_id','files','size','type_file'
    ];
}
