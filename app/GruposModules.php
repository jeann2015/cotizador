<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposModules extends Model
{
    protected $table="grupos_modules";

    /**
     * @param $grupo
     * @param $module
     * @return mixed
     */
    public function grupos_modulos($grupo, $module)
    {
        $gruposmodule = \DB::table('grupos_modules')
            ->where('id_grupos', '=', $grupo)
            ->where('id_modules', '=', $module)
            ->get();

        return $gruposmodule;
    }
}
