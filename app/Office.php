<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $table="offices";

    protected $fillable = [
        'name','address','schedule','lat','lon','phone'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function traders()
    {
        return $this->hasMany('App\OfficeTrader');
    }

    public function get_offices($id=0)
    {
        $offices = self::
        join('office_traders', function ($join) use ($id) {
            if ($id<>0) {
                $join->on('office_traders.offices_id', '=', 'offices.id')
                    ->where('office_traders.trade_marks_id', '=', $id);
            } else {
                $join->on('office_traders.offices_id', '=', 'offices.id');
            }
        })
            ->join('trade_marks', 'trade_marks.id', '=', 'office_traders.trade_marks_id')
            ->select('offices.*', 'trade_marks.description as description_trade_marks',
                'office_traders.trade_marks_id')
            ->get();

        return $offices;
    }
}
